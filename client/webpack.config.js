const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const NODE_MODULES_DIR = path.resolve(__dirname, 'node_modules');
const BUILD_DIR = path.resolve(__dirname, 'build');
const APP_DIR = path.resolve(__dirname, 'src');

const env = process.env.NODE_ENV || 'production';
const parameters = require('./config/parameters.json');

const plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify(env)
        },
        'PARAMETERS': {
            analyzer_domain: JSON.stringify(parameters.analyzer_domain),
            mock_api: JSON.stringify(parameters.mock_api),
        },
    }),
    new ExtractTextPlugin({
        filename: 'app.css',
        allChunks: true,
    }),
    new HtmlWebpackPlugin({
        template: './src/index.html',
        filename: 'index.html',
        inject: 'body'
    })
];

if (env === 'production') {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
        },
    }));
}

const config = {
  entry: [
    APP_DIR + '/index.js',
    APP_DIR + '/scss/app.scss'
  ],
  output: {
    path: BUILD_DIR,
    filename: 'app.bundle.js',
    hotUpdateChunkFilename: 'hot/hot-update.js',
    hotUpdateMainFilename: 'hot/hot-update.json'
  },
  module: {
    loaders: [{
      test: /.js?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'react','stage-0']
      }
    },
    {
      test: /\.scss$/,
      exclude: [NODE_MODULES_DIR],
      use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: ['css-loader','postcss-loader','sass-loader']
      }))
    }]
  },
  plugins,
  devServer: {
    host: 'localhost',
    port: 3000,
    historyApiFallback: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
  }
};

module.exports = config;
