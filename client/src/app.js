import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import History from "./containers/history";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={ History } />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
