const AVAILABLE_PERIODS = PARAMETERS.analyzer_domain + '/analyzer/get-available-periods';
const HISTORY_URL = PARAMETERS.analyzer_domain + '/analyzer/get-data';
const TRANSACTIONS_URL = PARAMETERS.analyzer_domain + '/analyzer/play-simulation';
const MOCKED_PORTFOLIO_URL = PARAMETERS.mock_api + '/portfolio';
const AVAILABLE_PAIRS = PARAMETERS.analyzer_domain + '/analyzer/get-available-pairs';

export {
  AVAILABLE_PERIODS,
  AVAILABLE_PAIRS,
  HISTORY_URL,
  MOCKED_PORTFOLIO_URL,
  TRANSACTIONS_URL,
};
