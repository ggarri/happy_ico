import React from "react";
import PropTypes from "prop-types";

import { format } from "d3-format";
import { timeFormat } from "d3-time-format";

import { ChartCanvas, Chart } from "react-stockcharts";
import {
  BarSeries,
  AreaSeries,
  CandlestickSeries,
  LineSeries,
  ScatterSeries,
  CircleMarker
} from "react-stockcharts/lib/series";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import {
  CrossHairCursor,
  EdgeIndicator,
  CurrentCoordinate,
  MouseCoordinateX,
  MouseCoordinateY,
} from "react-stockcharts/lib/coordinates";

import { discontinuousTimeScaleProvider } from "react-stockcharts/lib/scale";
import {
  OHLCTooltip,
  MovingAverageTooltip,
  HoverTooltip
} from "react-stockcharts/lib/tooltip";
import { ema, heikinAshi, sma } from "react-stockcharts/lib/indicator";
import { fitWidth } from "react-stockcharts/lib/helper";
import { last } from "react-stockcharts/lib/utils";

const dateFormat = timeFormat("%Y-%m-%d");
const numberFormat = format(".2f");

function tooltipContent(ys) {
  return ({ currentItem, xAccessor }) => {
    return {
      x: dateFormat(xAccessor(currentItem)),
      y: [
        {
          label: "Open",
          value: currentItem.open && numberFormat(currentItem.open)
        },
        {
          label: "Action",
          value: currentItem.action
        },
        {
          label: "Volume",
          value: numberFormat(currentItem.actionVolume)
        },
        {
          label: "Rate",
          value: numberFormat(currentItem.actionRate)
        },
        {
          label: "Fee",
          value: numberFormat(currentItem.actionFee)
        }
      ]
        .concat(
          ys.map(each => ({
            label: each.label,
            value: each.value(currentItem),
            stroke: each.stroke
          }))
        )
        .filter(line => line.value)
    };
  };
}

class HeikinAshi extends React.Component {
  render() {
    const ha = heikinAshi();
    const ema20 = ema()
      .id(0)
      .options({ windowSize: 20 })
      .merge((d, c) => { d.ema20 = c; })
      .accessor(d => d.ema20);

    const ema50 = ema()
      .id(2)
      .options({ windowSize: 50 })
      .merge((d, c) => { d.ema50 = c; })
      .accessor(d => d.ema50);

    const smaVolume50 = sma()
      .id(3)
      .options({ windowSize: 50, sourcePath: "volume" })
      .merge((d, c) => { d.smaVolume50 = c; })
      .accessor(d => d.smaVolume50);

    const { type, data: initialData, width, ratio } = this.props;

    const calculatedData = smaVolume50(ema50(ema20(ha(initialData))));
    const xScaleProvider = discontinuousTimeScaleProvider
      .inputDateAccessor(d => d.date);
    const {
      data,
      xScale,
      xAccessor,
      displayXAccessor,
    } = xScaleProvider(calculatedData);

    const start = xAccessor(last(data));
    const end = xAccessor(data[Math.max(0, data.length - 150)]);
    const xExtents = [start, end];

    const chartHeight = window.innerHeight - 50;

    return (
      <ChartCanvas height={chartHeight}
                   ratio={ratio}
                   width={width}
                   margin={{ left: 80, right: 80, top: 10, bottom: 30 }}
                   type={type}
                   data={data}
                   xScale={xScale}
                   xAccessor={xAccessor}
                   displayXAccessor={displayXAccessor}
                   xExtents={xExtents}
      >
        <Chart id={1}
               yExtents={[d => [d.high, d.low], ema20.accessor(), ema50.accessor()]}
               padding={{ top: 50, bottom: 20 }}
               height={chartHeight/2}
        >
          <XAxis axisAt="bottom" orient="bottom"/>
          <YAxis axisAt="right" orient="right" ticks={5} />
          <MouseCoordinateY
            at="right"
            orient="right"
            displayFormat={format(".1f")} />

          <CandlestickSeries />
          <LineSeries yAccessor={ema20.accessor()} stroke={ema20.stroke()}/>
          <LineSeries yAccessor={ema50.accessor()} stroke={ema50.stroke()}/>

          <CurrentCoordinate yAccessor={ema20.accessor()} fill={ema20.stroke()} />
          <CurrentCoordinate yAccessor={ema50.accessor()} fill={ema50.stroke()} />

          <ScatterSeries
            yAccessor={d => { return d.action === 'BUY' ? d.actionRate : null; }}
            marker={CircleMarker}
            markerProps={{ r: 8 , stroke: "#006400", fill: "#007f00"}}
          />

          <ScatterSeries
            yAccessor={d => { return d.action === 'SELL' ? d.actionRate : null; }}
            marker={CircleMarker}
            markerProps={{ r: 8 , stroke: "#8b0000", fill: "#ff0002"}}
          />

          <HoverTooltip
            yAccessor={ema50.accessor()}
            tooltipContent={tooltipContent([
              {
                label: `${ema20.type()}(${ema20.options()
                  .windowSize})`,
                value: d => numberFormat(ema20.accessor()(d)),
                stroke: ema20.stroke()
              },
              {
                label: `${ema50.type()}(${ema50.options()
                  .windowSize})`,
                value: d => numberFormat(ema50.accessor()(d)),
                stroke: ema50.stroke()
              }
            ])}
            fontSize={12}
          />

          <OHLCTooltip origin={[-40, 0]}/>
          <MovingAverageTooltip
            onClick={e => console.log(e)}
            origin={[-38, 15]}
            options={[
              {
                yAccessor: ema20.accessor(),
                type: "EMA",
                stroke: ema20.stroke(),
                windowSize: ema20.options().windowSize,
              },
              {
                yAccessor: ema50.accessor(),
                type: "EMA",
                stroke: ema50.stroke(),
                windowSize: ema50.options().windowSize,
              },
            ]}
          />
        </Chart>

        <Chart id={2}
               yExtents={[d => d.volume, smaVolume50.accessor()]}
               height={200} origin={(w, h) => [0, h - 200]}
        >
          <YAxis axisAt="left" orient="left" ticks={5} tickFormat={format(".2s")}/>
          <MouseCoordinateX
            at="bottom"
            orient="bottom"
            displayFormat={timeFormat("%Y-%m-%d")} />
          <MouseCoordinateY
            at="left"
            orient="left"
            displayFormat={format(".4s")} />

          <BarSeries yAccessor={d => d.volume} fill={d => d.close > d.open ? "#6BA583" : "#FF0000"} />
        </Chart>
        <CrossHairCursor />
      </ChartCanvas>
    );
  }
}

HeikinAshi.propTypes = {
  data: PropTypes.array.isRequired,
  width: PropTypes.number.isRequired,
  ratio: PropTypes.number.isRequired,
  type: PropTypes.oneOf(["svg", "hybrid"]).isRequired,
};

HeikinAshi.defaultProps = {
  type: "svg",
};

HeikinAshi = fitWidth(HeikinAshi);

export default HeikinAshi;
