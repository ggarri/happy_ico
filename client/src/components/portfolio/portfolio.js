import React, { PureComponent } from 'react';
import _ from 'lodash';
import axios from 'axios';
import { POST_PORTFOLIO_ALGHT_TRANSACTIONS_URL } from '../../urls';

class Portfolio extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      portfolio: props.portfolio,
      id: props.id,
      hideButton: props.hideButton ? props.hideButton : false
    }
  }

  inputChange = (nextValue, currency) => {
    let { wallet } = this.state.portfolio;

    wallet[currency] = +nextValue;
    this.setState({ wallet });
    this.forceUpdate();
  };

  handleSubmit = () => {
    this.props.update(this.state.portfolio);
  };

  render() {
    const { wallet } = this.state.portfolio;
    const { hideButton } = this.state;
    const exchangeCurrency = 'EUR';

    return (
      <form>
        {
          _.map(this.props.pairs, (pair) => {
            const currency = pair.substring(0, 3);
            return <p>
              <label htmlFor={ currency } className="u-margin-right--small">{ currency }</label>
              <input
                type="text"
                id={ currency }
                value={ wallet[currency] || 0 }
                onChange={ (e) => this.inputChange(e.target.value, currency) }
              />
            </p>
          })

        }
        <p>
          <label htmlFor={ exchangeCurrency } className="u-margin-right--small">{ exchangeCurrency }</label>
          <input
            type="text"
            id={ exchangeCurrency }
            value={ wallet[exchangeCurrency] || 0 }
            onChange={ (e) => this.inputChange(e.target.value, exchangeCurrency) }
          />
        </p>
        <p><strong>Total:</strong> {this.state.portfolio.total_in_exchange_currency} {this.state.portfolio.exchange_currency}</p>
        { !hideButton ? <button type="button" onClick={ this.handleSubmit }>Load</button> : null }
      </form>
    );
  }
}

export default Portfolio;
