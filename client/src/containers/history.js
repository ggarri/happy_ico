import React, { Component } from 'react';
import { Chart, Portfolio } from '../components';

import _ from 'lodash';
import axios from 'axios';
import moment from 'moment';
import { Calendar } from 'react-date-range';
import { HISTORY_URL, AVAILABLE_PERIODS, MOCKED_PORTFOLIO_URL, TRANSACTIONS_URL, AVAILABLE_PAIRS } from '../urls';

const dateTransactionsFormat = 'YYYYMMDDhhmm';

class History extends Component {
  state = {
    history: [],
    availablePairs: [],
    pair: 'ETHEUR',
    interval: 60,
    from: null,
    now: null,
    intervals: [5, 10, 15, 30, 60, 120],
    transactions: [],
    originPortfolio: [],
    nextPortfolio: [],
    candidatePortfolio: [],
    availablePeriods: [],
    showSinceCalendar: false,
    showNowCalendar: false
  };

  dateToReadableFormat(date) {
    let formattedDate = '';
    formattedDate += date.toString().substring(0, 4) + '-';
    formattedDate += date.toString().substring(4, 6) + '-';
    formattedDate += date.toString().substring(6, 8) + ' ';
    formattedDate += date.toString().substring(8, 10) + ':';
    formattedDate += date.toString().substring(10, 12);

    return formattedDate;
  }

  timeToDate = (time) => {
    return new Date(time * 1000);
  };

  dateToTime = (date) => {
    return new Date(date).getTime() / 1000;
  };

  getAvailablePairs = () => {
    axios.get(
      AVAILABLE_PAIRS
    ).then((res) => {
      this.loadAvailablePairs(res.data);
    })
      .catch(function(res) {
        if(res instanceof Error) {
          console.log(res.message);
        } else {
          console.log(res.data);
        }
      });
  };

  loadAvailablePairs = (availablePairs) => {
    this.setState({ availablePairs })
  };

  getHistory = () => {
    const params = {
      pair: this.state.pair,
      from: this.state.from,
      now: this.state.now,
      interval: this.state.interval
    };

    axios.get(
      HISTORY_URL,
      { params }
    ).then((res) => {
      this.loadHistory(res.data);
    })
    .catch(function(res) {
      if(res instanceof Error) {
        console.log(res.message);
      } else {
        console.log(res.data);
      }
    });
  };

  loadHistory = (data) => {
    let history = [];

    this.setState({history: []});

    _.each(data, (item) => {
      let date = this.timeToDate(item['time']);
      let open = item['open'];
      let close = item['close'];
      let high = item['high'];
      let low = item['low'];
      let volume = item['volume'];

      history.push({date, open, close, high, low, volume});
    });

    this.setState({history});
  };

  getOriginPortfolio = () => {
    axios.get(
      MOCKED_PORTFOLIO_URL
    ).then((res) => {
        this.setState({ originPortfolio: res.data });
      })
      .catch((res) => {
        if(res instanceof Error) {
          console.log(res.message);
        } else {
          console.log(res.data);
        }
      });
  };

  handleOriginPortfolio = () => {
    this.resetPortfolios();
    this.getTransactions();
  };

  resetPortfolios = () => {
    this.setState({nextPortfolio: []});
    this.setState({candidatePortfolio: []});
  };

  getTransactions = () => {
    const params = {
      from: this.state.from,
      now: this.state.now,
      portfolio: this.state.originPortfolio
    };

    axios.get(
      TRANSACTIONS_URL,
      { params }
    ).then(function (res) {
      this.loadTransactions(res.data);
      this.loadPortfolios(res.data);
    }.bind(this))
      .catch(function(res) {
        if(res instanceof Error) {
          console.log(res.message);
        } else {
          console.log(res.data);
        }
      });
  };

  loadPortfolios(data) {
    this.setState({ originPortfolio: data.orig_portfolio });
    this.setState({ nextPortfolio: data.next_portfolio });
    this.setState({ candidatePortfolio: data.candidate_portfolio });
  }

  loadTransactions = (data) => {
    let { history } = this.state;

    _.each(data.trxs, (transaction) => {

      const historyItem = _.find(history, (item) => {
        let historyItemDate = moment(item.date).format(dateTransactionsFormat);
        let transactionDate = moment(this.timeToDate(transaction['timestamp'])).format(dateTransactionsFormat);
        return historyItemDate == transactionDate }
      );

      historyItem.action = transaction['action'].toUpperCase();
      historyItem.actionRate = transaction['rate'];
      historyItem.actionVolume = transaction['volume'];
      historyItem.actionFee = transaction['fee'];
    });

    this.setState({history});
  };

  changePair = (nextValue) => {
    this.setState({pair: nextValue}, this.getAvailablePeriods);
  };

  changeInterval = (nextValue) => {
    this.setState({interval: nextValue}, this.getAvailablePeriods);
  };

  updateChart = () => {
    this.resetPortfolios();
    this.getHistory();
  };

  getAvailablePeriods() {
    const params = {
      pair: this.state.pair,
      interval: this.state.interval
    };

    axios.get(
      AVAILABLE_PERIODS,
      { params }
    ).then((res) => {
        this.setState({ availablePeriods: res.data });
        this.setState({ from: moment(res.data[0]).format(dateTransactionsFormat) });
        this.setState({ now: moment(res.data[1]).format(dateTransactionsFormat) });
      })
      .catch((res) => {
        if(res instanceof Error) {
          console.log(res.message);
        } else {
          console.log(res.data);
        }
      });
  }

  componentWillMount() {
    this.getAvailablePeriods();
    this.getAvailablePairs();
    //this.getHistory();
    this.getOriginPortfolio();
  }

  toggleSinceCalendar() {
    this.setState({showSinceCalendar: !this.state.showSinceCalendar});
  }

  handleSinceCalendar(nextValue){
    this.toggleSinceCalendar();
    this.setState({ from: moment(nextValue).format(dateTransactionsFormat) });
  }

  toggleNowCalendar() {
    this.setState({showNowCalendar: !this.state.showNowCalendar});
  }

  handleNowCalendar(nextValue){
    this.toggleNowCalendar();
    this.setState({ now: moment(nextValue).format(dateTransactionsFormat) });
  }

  render() {
    const {
      showSinceCalendar,
      showNowCalendar,
      availablePairs,
      history,
      originPortfolio,
      nextPortfolio,
      candidatePortfolio,
      pair,
      from,
      now,
      availablePeriods,
      interval,
      intervals
    } = this.state;

    if (availablePeriods.length === 0 && availablePairs.length === 0) {
      return <div>Loading...</div>
    }

    return (
      <article>
        <section className="u-padding--small u-width--85 u-display--inline-block u-float--left u-border--grey-light">
          { from ?
            <div className="u-display--inline-block u-position--relative">
              <label className="u-margin-right--small">
                <input type="text" readOnly={true}
                     value={this.dateToReadableFormat(from)}
                     onClick={this.toggleSinceCalendar.bind(this)}
                />
              </label>
              <div className={!showSinceCalendar ? 'u-display--none' : 'u-position--absolute u-width--100'}>
                <Calendar
                  date={from}
                  format={dateTransactionsFormat}
                  minDate={availablePeriods[0]}
                  maxDate={now}
                  onChange={this.handleSinceCalendar.bind(this)}
                />
              </div>
            </div>
            : null }
          { now ?
            <div className="u-display--inline-block u-position--relative">
              <label>
                <input type="text" readOnly={true}
                     className="u-margin-right--small"
                     value={this.dateToReadableFormat(now)}
                     onClick={this.toggleNowCalendar.bind(this)}
                />
              </label>
              <div className={!showNowCalendar ? 'u-display--none' : 'u-position--absolute u-width--100'}>
                <Calendar
                  date={now}
                  format={dateTransactionsFormat}
                  minDate={from}
                  maxDate={availablePeriods[1]}
                  onChange={this.handleNowCalendar.bind(this)}
                />
              </div>
            </div>
            : null }
          <label className="u-margin-right--small">
            <select id="interval" value={interval} onChange={e => this.changeInterval(e.target.value)}>
              {
                _.map(intervals, (interval) => <option value={interval}>{interval}</option>)
              }
            </select>
          </label>
          <label className="u-margin-right--small">
            <select id="pair" value={pair} className="u-margin-right--small" onChange={e => this.changePair(e.target.value)}>
              {
                _.map(availablePairs, (pair) => <option value={pair}>{pair}</option>)
              }
            </select>
          </label>
          <button type="button" onClick={this.updateChart}>Update chart</button>
        </section>

        <section className="u-width--85 u-display--inline-block u-float--left">
          { history.length !== 0 ? <Chart data={history} /> : null }
        </section>

        <section className="u-width--15 u-display--inline-block">
          <div className="u-border--grey-light u-padding--small u-margin-bottom--medium">
            <h2 className="u-font-size--base">Origin Portfolio</h2>
            { originPortfolio.length !== 0 ? <Portfolio update={this.handleOriginPortfolio} portfolio={originPortfolio} pairs={availablePairs} /> : null }
          </div>
          <div className="u-border--grey-light u-padding--small u-margin-bottom--medium">
            <h2 className="u-font-size--base">Next Portfolio</h2>
            { nextPortfolio.length !== 0 ? <Portfolio portfolio={nextPortfolio} pairs={availablePairs} hideButton="true" /> : null }
          </div>
          <div className="u-border--grey-light u-padding--small">
            <h2 className="u-font-size--base">Candidate Portfolio</h2>
            { candidatePortfolio.length !== 0 ? <Portfolio portfolio={candidatePortfolio} pairs={availablePairs} hideButton="true" /> : null }
          </div>
        </section>
      </article>
    );
  }
}

export default History;