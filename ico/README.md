## Set environment

### OS Dependencies
Install next OS dependencies for your OS

- apache
- php
- mysql
- composer
- php-xml
- php-mysql
- php-curl


### Start services
Example:
```
$ systemctl start cronie
$ systemctl start mysqld
```

### Install vendors
composer install

### Set ico/app/config/parameters.yml
Example:
```
parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: ico
    database_user: root
    database_password: null
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: ThisTokenIsNotSoSecretChangeIt
    
    ico.api_key: XXXXXXXXXXXXX
    ico.api_secret: XXXXXXXXXXXX
    ico.api_url: https://api.kraken.com
    ico.api_version: 0
    ico.api_sslverify: true
    
    ico.pair_past_import:
        ETHEUR:
            intervals:
                - 5
                - 15
                - 60
        BTCEUR:
            intervals:
                - 5
                - 15
                - 60
        XRPEUR:
            intervals:
                - 5
                - 15
                - 60
    ico.pair_current_import:
        ETHEUR:
            intervals:
                - 5
                - 15
        BTCEUR:
            intervals:
                - 5
                - 15
        XRPEUR:
            intervals:
                - 5
                - 15
```

### Install vendors

```
$ composer install
```

### Create schema DB

```
$ php bin/console doctrine:schema:create
```

### Update schema DB

```
$ php bin/console doctrine:schema:update --force
```

## How to use it

### API end points

```
/get-pairs
 ```
 
```
/get-since
 ```
 
```
/get-ohlc/?pair=ETHEUR[&interval=1&since=20171013000000]
 ```
 
```
/get-ticker/?pair=ETHEUR[&since=20171013000000]
 ```

### Run command to import past history
```
$ php bin/console ico:import:history
```

### Run command to import current history
```
$ php bin/console ico:import:current
```

### Configure Cron
Execute:
```
$ crontab -e
```
And edit it adding:
```
@hourly /usr/bin/php /home/FOLDER_TO_THE PROJECT/happy_ico/ico/bin/console ico:import:current
``````
