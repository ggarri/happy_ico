<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/22/17
 * Time: 11:56 AM
 */

namespace AppBundle\Command;

class ImportCurrentTimeCommand extends AbstractHistoryCommand
{
    protected function getCommandName()
    {
        return 'ico:import:current';
    }
    protected function getCommandDescription()
    {
        return 'Import current data';
    }
    protected function getCommandHelp()
    {
        return 'Import current data';
    }

    /**
     * @param $sPair
     * @param $iInterval
     * @param $fIntervalAskBid
     * @param null $iSince
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function importHistory($sPair, $iInterval, $fIntervalAskBid, $iSince = null)
    {
        $this->oHistoryService->importCurrentHistory($sPair, $iInterval, $fIntervalAskBid);
    }

    protected function getConfigParam()
    {
        return 'ico.pair_current_import';
    }
}