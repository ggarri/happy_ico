<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/22/17
 * Time: 11:58 AM
 */

namespace AppBundle\Command;

use AppBundle\Component\Helper\DateTime;
use AppBundle\Service\HistoryService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\Container;

abstract class AbstractHistoryCommand extends Command
{
    const STATUS_SUCCESS = 0;
    const STATUS_ERROR = 1;

    /** @var  HistoryService */
    protected $oHistoryService;

    /** @var  array */
    protected $aPairs;

    protected function configure()
    {
        $this
            ->setName($this->getCommandName())
            ->setDescription($this->getCommandDescription())
            ->setHelp($this->getCommandHelp())
            ->setDefinition(
                new InputDefinition([
                    new InputOption('pair', 'p', InputOption::VALUE_OPTIONAL, 'Pair to import'),
                    new InputOption('interval', 'i', InputOption::VALUE_OPTIONAL, 'Interval between items'),
                    new InputOption('intervalAskBid', 'iai', InputOption::VALUE_OPTIONAL, 'Interval grouping asks and bids'),
                    new InputOption('since', 's', InputOption::VALUE_OPTIONAL, 'Timestamp to start importing data')
                ]));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init();

        $_sPair = strtoupper($input->getOption('pair'));
        $_iInterval = $input->getOption('interval');
        $_fIntervalAskBid = $input->getOption('intervalAskBid');
        $_iSince = $input->getOption('since');

        $output->writeln('Import history started at  ' . date(DateTime::HUMAN_FORMAT));

        try
        {
            if ($_sPair)
            {
                $this->importHistory($_sPair, $_iInterval, $_fIntervalAskBid, $_iSince);
                return static::STATUS_SUCCESS;
            }

            foreach ($this->aPairs as $_sPair => $_aPairInfo)
            {
                $_aIntervals = isset($_aPairInfo['intervals'])
                    ? $_aPairInfo['intervals']
                    : $this->oHistoryService->getIntervals();

                foreach ($_aIntervals as $_iInterval)
                {
                    $output->writeln(date(DateTime::HUMAN_FORMAT). ' Inserting metrics for ' . $_sPair . ', interval = ' . $_iInterval);
                    $this->importHistory($_sPair, $_iInterval, null);
                }
            }
        }
        catch(\RuntimeException $_oE)
        {
            sprintf('Error: ' . $_oE->getMessage());
            return static::STATUS_ERROR;
        }

        $output->writeln('Import history finished at  ' . date(DateTime::HUMAN_FORMAT));
        return static::STATUS_SUCCESS;
    }

    protected function init()
    {
        /** @var Container $_oContainer */
        $_oContainer = $this->getApplication()->getKernel()->getContainer();
        $this->oHistoryService = $_oContainer->get(HistoryService::class);
        $this->aPairs = $_oContainer->getParameter($this->getConfigParam());
    }

    abstract protected function importHistory($sPair, $iInterval, $fIntervalAskBid, $iSince = null);
    abstract protected function getConfigParam();
    abstract protected function getCommandName();
    abstract protected function getCommandDescription();
    abstract protected function getCommandHelp();
}
