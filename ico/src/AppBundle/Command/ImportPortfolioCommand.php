<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/2/17
 * Time: 11:53 PM
 */

namespace AppBundle\Command;
use AppBundle\Component\Helper\DateTime;
use AppBundle\Service\PortfolioService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

class ImportPortfolioCommand extends Command
{
    /** @var  PortfolioService */
    protected $oPortfolioService;

    protected function configure()
    {
        $this
            ->setName('ico:import:portfolio')
            ->setDescription('Import portfolio')
            ->setHelp('Import portfolio');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Container $_oContainer */
        $_oContainer = $this->getApplication()->getKernel()->getContainer();
        $this->oPortfolioService = $_oContainer->get(PortfolioService::class);
        $output->writeln('Import history started at  ' . date(DateTime::HUMAN_FORMAT));

        try
        {
            $this->oPortfolioService->importPortfolio();
        }
        catch(\RuntimeException $_oE)
        {
            sprintf('Error: ' . $_oE->getMessage());
            return 1;
        }

        $output->writeln('Import history finished at  ' . date(DateTime::HUMAN_FORMAT));
        return 0;
    }
}