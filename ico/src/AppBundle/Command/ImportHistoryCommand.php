<?php


namespace AppBundle\Command;

/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/21/17
 * Time: 12:37 PM
 */
class ImportHistoryCommand extends AbstractHistoryCommand
{
    protected function getCommandName()
    {
        return 'ico:import:history';
    }
    protected function getCommandDescription()
    {
        return 'Import past data';
    }
    protected function getCommandHelp()
    {
        return 'Import past data';
    }

    /**
     * @param $sPair
     * @param $iInterval
     * @param $fIntervalAskBid
     * @param null $iSince
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function importHistory($sPair, $iInterval, $fIntervalAskBid, $iSince = null)
    {
        $this->oHistoryService->importPastHistory($sPair, $iInterval, $fIntervalAskBid, $iSince);
    }

    protected function getConfigParam()
    {
        return 'ico.pair_past_import';
    }
}