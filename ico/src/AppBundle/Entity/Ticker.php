<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 20/12/17
 * Time: 20:19
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TickerRepository")
 * @ORM\Table(name="ticker")
 */
class Ticker
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue
     * @JMS\SerializedName("id")
     */
    protected $iId;

    /**
     * @ORM\Column(type="datetime", name="time")
     * @var \DateTime
     * @JMS\SerializedName("time")
     */
    protected $iTime;

    /**
     * @ORM\Column(type="string", length=6, name="pair")
     * @var string
     * @JMS\SerializedName("pair")
     */
    protected $sPair;

    /**
     * ask array(<price>, <whole lot volume>, <lot volume>)
     * @ORM\Column(type="string", length=80, name="ask")
     * @var string
     * @JMS\SerializedName("ask")
     */
    protected $sAsk;

    /**
     * bid array(<price>, <whole lot volume>, <lot volume>)
     * @ORM\Column(type="string", length=80, name="bid")
     * @var string
     * @JMS\SerializedName("bid")
     */
    protected $sBid;

    /**
     * last trade closed array(<price>, <lot volume>)
     * @ORM\Column(type="string", length=80, name="last_trade")
     * @var string
     * @JMS\SerializedName("lastTrade")
     */
    protected $sLastTrade;

    /**
     * volume array(<today>, <last 24 hours>)
     * @ORM\Column(type="string", length=80, name="volume")
     * @var string
     * @JMS\SerializedName("volume")
     */
    protected $sVolume;

    /**
     * volume weighted average price array(<today>, <last 24 hours>)
     * @ORM\Column(type="string", length=80, name="volume_weighted_average")
     * @var string
     * @JMS\SerializedName("volumeWeightedAverage")
     */
    protected $sVolumeWeightedAverage;

    /**
     * number of trades array(<today>, <last 24 hours>)
     * @ORM\Column(type="string", length=80, name="number_trades")
     * @var string
     * @JMS\SerializedName("numberTrades")
     */
    protected $sNumberTrades;

    /**
     * low array(<today>, <last 24 hours>)
     * @ORM\Column(type="string", length=80, name="low")
     * @var string
     * @JMS\SerializedName("low")
     */
    protected $sLow;

    /**
     * high array(<today>, <last 24 hours>)
     * @ORM\Column(type="string", length=80, name="high")
     * @var string
     * @JMS\SerializedName("high")
     */
    protected $sHigh;

    /**
     * today's opening price
     * @ORM\Column(type="float", scale=6, name="today_open_price")
     * @var float
     * @JMS\SerializedName("todayOpenPrice")
     */
    protected $fTodayOpenPrice;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->iId;
    }

    /**
     * @param mixed $iId
     */
    public function setId($iId)
    {
        $this->iId = $iId;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->iTime;
    }

    /**
     * @param \DateTime $iTime
     */
    public function setTime($iTime)
    {
        $this->iTime = $iTime;
    }

    /**
     * @return string
     */
    public function getPair()
    {
        return $this->sPair;
    }

    /**
     * @param string $sPair
     */
    public function setPair($sPair)
    {
        $this->sPair = $sPair;
    }

    /**
     * @return string
     */
    public function getAsk()
    {
        return $this->sAsk;
    }

    /**
     * @param string $sAsk
     */
    public function setAsk($sAsk)
    {
        $this->sAsk = $sAsk;
    }

    /**
     * @return string
     */
    public function getBid()
    {
        return $this->sBid;
    }

    /**
     * @param string $sBid
     */
    public function setBid($sBid)
    {
        $this->sBid = $sBid;
    }

    /**
     * @return string
     */
    public function getLastTrade()
    {
        return $this->sLastTrade;
    }

    /**
     * @param string $sLastTrade
     */
    public function setLastTrade($sLastTrade)
    {
        $this->sLastTrade = $sLastTrade;
    }

    /**
     * @return string
     */
    public function getVolume()
    {
        return $this->sVolume;
    }

    /**
     * @param string $sVolume
     */
    public function setVolume($sVolume)
    {
        $this->sVolume = $sVolume;
    }

    /**
     * @return string
     */
    public function getVolumeWeightedAverage()
    {
        return $this->sVolumeWeightedAverage;
    }

    /**
     * @param string $sVolumeWeightedAverage
     */
    public function setVolumeWeightedAverage($sVolumeWeightedAverage)
    {
        $this->sVolumeWeightedAverage = $sVolumeWeightedAverage;
    }

    /**
     * @return string
     */
    public function getNumberTrades()
    {
        return $this->sNumberTrades;
    }

    /**
     * @param string $sNumberTrades
     */
    public function setNumberTrades($sNumberTrades)
    {
        $this->sNumberTrades = $sNumberTrades;
    }

    /**
     * @return string
     */
    public function getLow()
    {
        return $this->sLow;
    }

    /**
     * @param string $sLow
     */
    public function setLow($sLow)
    {
        $this->sLow = $sLow;
    }

    /**
     * @return string
     */
    public function getHigh()
    {
        return $this->sHigh;
    }

    /**
     * @param string $sHigh
     */
    public function setHigh($sHigh)
    {
        $this->sHigh = $sHigh;
    }

    /**
     * @return float
     */
    public function getTodayOpenPrice()
    {
        return $this->fTodayOpenPrice;
    }

    /**
     * @param float $fTodayOpenPrice
     */
    public function setTodayOpenPrice($fTodayOpenPrice)
    {
        $this->fTodayOpenPrice = $fTodayOpenPrice;
    }
}