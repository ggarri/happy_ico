<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OhlcRepository")
 * @ORM\Table(name="ohlc", uniqueConstraints={@ORM\UniqueConstraint(name="time_pair_interval_idx", columns={"time", "pair", "period"})})
 */
class Ohlc
{
    CONST TABLE_NAME = 'ohlc';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue
     * @JMS\SerializedName("id")
     */
    protected $iId;
    
    /**
     * @ORM\Column(type="datetime", name="time")
     * @var \DateTime
     * @JMS\SerializedName("time")
     */
    protected $iTime;

    /**
     * @ORM\Column(type="string", length=6, name="pair")
     * @var string
     * @JMS\SerializedName("pair")
     */
    protected $sPair;

    /**
     * @ORM\Column(type="integer", name="period")
     * @var int
     * @JMS\SerializedName("interval")
     */
    protected $iInterval;

    /**
     * @ORM\Column(type="float", scale=6, name="low")
     * @var float
     * @JMS\SerializedName("low")
     */
    protected $fLow;

    /**
     * @ORM\Column(type="float", scale=6, name="high")
     * @var float
     * @JMS\SerializedName("high")
     */
    protected $fHigh;

    /**
     * @ORM\Column(type="float", scale=6, name="open")
     * @var float
     * @JMS\SerializedName("open")
     */
    protected $fOpen;

    /**
     * @ORM\Column(type="float", scale=6, name="close")
     * @var float
     * @JMS\SerializedName("close")
     */
    protected $fClose;

    /**
     * @ORM\Column(type="float", scale=6, name="volume")
     * @var float
     * @JMS\SerializedName("volume")
     */
    protected $fVolume;

    /**
     * @ORM\Column(type="blob", name="bids", nullable=true)
     * @var string
     * @JMS\SerializedName("bids")
     */
    protected $rBids;

    /**
     * @ORM\Column(type="blob", name="asks", nullable=true)
     * @var string
     * @JMS\SerializedName("asks")
     */
    protected $rAsks;


    /**
     * @return string
     */
    public function getTime()
    {
        return $this->iTime;
    }

    /**
     * @param \DateTime $iTime
     */
    public function setTime($iTime)
    {
        $this->iTime = $iTime;
    }

    /**
     * @return string
     */
    public function getPair()
    {
        return $this->sPair;
    }

    /**
     * @param string $sPair
     */
    public function setPair($sPair)
    {
        $this->sPair = $sPair;
    }

    /**
     * @return float
     */
    public function getLow()
    {
        return $this->fLow;
    }

    /**
     * @param float $fLow
     */
    public function setLow($fLow)
    {
        $this->fLow = $fLow;
    }

    /**
     * @return float
     */
    public function getHigh()
    {
        return $this->fHigh;
    }

    /**
     * @param float $fHigh
     */
    public function setHigh($fHigh)
    {
        $this->fHigh = $fHigh;
    }

    /**
     * @return float
     */
    public function getInterval()
    {
        return $this->iInterval;
    }

    /**
     * @param float $iInterval
     */
    public function setInterval($iInterval)
    {
        $this->iInterval = $iInterval;
    }

    /**
     * @return float
     */
    public function getOpen()
    {
        return $this->fOpen;
    }

    /**
     * @param float $fOpen
     */
    public function setOpen($fOpen)
    {
        $this->fOpen = $fOpen;
    }

    /**
     * @return float
     */
    public function getClose()
    {
        return $this->fClose;
    }

    /**
     * @param float $fClose
     */
    public function setClose($fClose)
    {
        $this->fClose = $fClose;
    }

    /**
     * @return float
     */
    public function getVolume()
    {
        return $this->fVolume;
    }

    /**
     * @param float $fVolume
     */
    public function setVolume($fVolume)
    {
        $this->fVolume = $fVolume;
    }

    /**
     * @return string
     */
    public function getBids()
    {
        return $this->rBids;
    }

    /**
     * @param string $rBids
     */
    public function setBids($rBids)
    {
        $this->rBids = $rBids;
    }

    /**
     * @return string
     */
    public function getAsks()
    {
        return $this->rAsks;
    }

    /**
     * @param string $rAsks
     */
    public function setAsks($rAsks)
    {
        $this->rAsks = $rAsks;
    }
}
