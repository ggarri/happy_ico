<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/21/17
 * Time: 11:50 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PortfolioRepository")
 * @ORM\Table(name="portfolio")
 */
class Portfolio
{
    const TABLE_NAME = 'portfolio';

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50, name="ordertxid")
     * @var string
     */
    protected $sOrdertxid;

    /**
     * @ORM\Column(type="string", length=10, name="pair")
     * @var string
     */
    protected $sPair;

    /**
     * @ORM\Column(type="float", scale=4, name="time")
     * @var float
     */
    protected $iTime;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=1, name="type")
     * @var string
     */
    protected $sType;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10, name="order_type")
     * @var string
     */
    protected $sOrdertype;

    /**
     * @ORM\Column(type="decimal", scale=6, name="price")
     * @var float
     */
    protected $fPrice;

    /**
     * @ORM\Column(type="decimal", scale=6, name="cost")
     * @var float
     */
    protected $fCost;

    /**
     * @ORM\Column(type="decimal", scale=6, name="fee")
     * @var float
     */
    protected $fFee;

    /**
     * @ORM\Column(type="decimal", scale=6, name="vol")
     * @var float
     */
    protected $fVol;

    /**
     * @ORM\Column(type="decimal", scale=6, name="margin")
     * @var float
     */
    protected $fMargin;

    /**
     * @return string
     */
    public function getOrdertxid()
    {
        return $this->sOrdertxid;
    }

    /**
     * @param string $sOrdertxid
     */
    public function setOrdertxid($sOrdertxid)
    {
        $this->sOrdertxid = $sOrdertxid;
    }

    /**
     * @return string
     */
    public function getPair()
    {
        return $this->sPair;
    }

    /**
     * @param string $sPair
     */
    public function setPair($sPair)
    {
        $this->sPair = $sPair;
    }

    /**
     * @return float
     */
    public function getTime()
    {
        return $this->iTime;
    }

    /**
     * @param float $iTime
     */
    public function setTime($iTime)
    {
        $this->iTime = $iTime;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->sType;
    }

    /**
     * @param string $sType
     */
    public function setType($sType)
    {
        $this->sType = $sType;
    }

    /**
     * @return string
     */
    public function getOrdertype()
    {
        return $this->sOrdertype;
    }

    /**
     * @param string $sOrdertype
     */
    public function setOrdertype($sOrdertype)
    {
        $this->sOrdertype = $sOrdertype;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->fPrice;
    }

    /**
     * @param float $fPrice
     */
    public function setPrice($fPrice)
    {
        $this->fPrice = $fPrice;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->fCost;
    }

    /**
     * @param float $fCost
     */
    public function setCost($fCost)
    {
        $this->fCost = $fCost;
    }

    /**
     * @return float
     */
    public function getFee()
    {
        return $this->fFee;
    }

    /**
     * @param float $fFee
     */
    public function setFee($fFee)
    {
        $this->fFee = $fFee;
    }

    /**
     * @return float
     */
    public function getVol()
    {
        return $this->fVol;
    }

    /**
     * @param float $fVol
     */
    public function setVol($fVol)
    {
        $this->fVol = $fVol;
    }

    /**
     * @return float
     */
    public function getMargin()
    {
        return $this->fMargin;
    }

    /**
     * @param float $fMargin
     */
    public function setMargin($fMargin)
    {
        $this->fMargin = $fMargin;
    }

    static public function buildPortfolios($aPortfolios)
    {
        $_aPortfolios = [];
        foreach ($aPortfolios as $aPortfolio)
        {
            $_aPortfolios[] = static::buildPortfolio($aPortfolio);
        }

        return $_aPortfolios;
    }

    static public function buildPortfolio($aPortfolio)
    {
        $_oPortfolio = new Portfolio();
        $_oPortfolio->setOrdertxid($aPortfolio['ordertxid']);
        $_oPortfolio->setPair($aPortfolio['pair']);
        $_oPortfolio->setTime($aPortfolio['time']);
        $_oPortfolio->setType($aPortfolio['type']);
        $_oPortfolio->setOrdertype($aPortfolio['ordertype']);
        $_oPortfolio->setPrice($aPortfolio['price']);
        $_oPortfolio->setCost($aPortfolio['cost']);
        $_oPortfolio->setFee($aPortfolio['fee']);
        $_oPortfolio->setVol($aPortfolio['vol']);
        $_oPortfolio->setMargin($aPortfolio['margin']);

        return $_oPortfolio;
    }

    static public function portfoliosToArray($aPortfolios)
    {
        $_aPortfolios = [];
        foreach ($aPortfolios as $oPortfolio)
        {
            $_aPortfolios[] = static::portfolioToArray($oPortfolio);
        }

        return $_aPortfolios;
    }

    /**
     * @param Portfolio $oPortfolio
     * @return array
     */
    static public function portfolioToArray($oPortfolio)
    {
        $_aPortfolio = [];
        $_aPortfolio['ordertxid'] = $oPortfolio->getOrdertxid();
        $_aPortfolio['pair'] = $oPortfolio->getPair();
        $_aPortfolio['time'] = $oPortfolio->getTime();
        $_aPortfolio['type'] = $oPortfolio->getType();
        $_aPortfolio['ordertype'] = $oPortfolio->getOrdertype();
        $_aPortfolio['price'] = $oPortfolio->getPrice();
        $_aPortfolio['cost'] = $oPortfolio->getCost();
        $_aPortfolio['fee'] = $oPortfolio->getFee();
        $_aPortfolio['vol'] = $oPortfolio->getVol();
        $_aPortfolio['margin'] = $oPortfolio->getMargin();

        return $_aPortfolio;
    }
}
