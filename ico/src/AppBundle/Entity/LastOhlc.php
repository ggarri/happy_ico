<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/1/17
 * Time: 12:03 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LastOhlcRepository")
 * @ORM\Table(name="last_ohlc")
 */
class LastOhlc
{
    const TABLE_NAME = 'last_ohlc';

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10, name="pair")
     * @var string
     */
    protected $sPair;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="period")
     * @var int
     */
    protected $iInterval;

    /**
     * @ORM\Column(type="float", scale=4, name="time")
     * @var float
     */
    protected $fTime;

    /**
     * @return string
     */
    public function getPair()
    {
        return $this->sPair;
    }

    /**
     * @param string $sPair
     */
    public function setPair($sPair)
    {
        $this->sPair = $sPair;
    }

    /**
     * @return float
     */
    public function getTime()
    {
        return $this->fTime;
    }

    /**
     * @param float $fTime
     */
    public function setTime($fTime)
    {
        $this->fTime = $fTime;
    }

    /**
     * @return int
     */
    public function getInterval()
    {
        return $this->iInterval;
    }

    /**
     * @param int $iInterval
     */
    public function setInterval($iInterval)
    {
        $this->iInterval = $iInterval;
    }
}
