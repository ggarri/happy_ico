<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/21/17
 * Time: 8:00 PM
 */

namespace AppBundle\Controller;

use AppBundle\Component\Http\JsonResponse;
use AppBundle\Service\HistoryService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HistoryController extends Controller
{
    /**
     * @Route("/get-ohlc", name="get-ohlc")
     * @param Request $oRequest
     * @return JsonResponse
     */
    public function ohlcAction(Request $oRequest)
    {
        $_sPair = $oRequest->query->get('pair');
        $_iInterval = $oRequest->query->get('interval');
        $_sSince = $oRequest->query->get('since');

        if (is_null($_sPair))
        {
            return new JsonResponse('Param pair is required');
        }

        /** @var HistoryService $_oHistoryService */
        $_oHistoryService = $this->get(HistoryService::class);
        $_aHistory = $_oHistoryService->getOhlc(strtoupper($_sPair), $_iInterval, $_sSince);
        return new JsonResponse($_aHistory);
    }

    /**
     * @Route("/get-ticker", name="get-ticker")
     * @param Request $oRequest
     * @return JsonResponse
     */
    public function tickerAction(Request $oRequest)
    {
        $_sPair = $oRequest->query->get('pair');
        $_sSince = $oRequest->query->get('since');

        if (is_null($_sPair))
        {
            return new JsonResponse('Param pair is required');
        }

        /** @var HistoryService $_oHistoryService */
        $_oHistoryService = $this->get(HistoryService::class);
        $_aHistory = $_oHistoryService->getTicker(strtoupper($_sPair), $_sSince);
        return new JsonResponse($_aHistory, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @Route("/get-pairs", name="get-pairs")
     * @param Request $oRequest
     * @return JsonResponse
     */
    public function getPairsAction(Request $oRequest)
    {
        /** @var HistoryService $_oHistoryService */
        $_oHistoryService = $this->get(HistoryService::class);
        $_aAvailablePairs = $_oHistoryService->getAvailablePairs();
        return new JsonResponse($_aAvailablePairs);
    }

    /**
     * @Route("/get-since", name="get-since")
     * @param Request $oRequest
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSinceAction(Request $oRequest)
    {
        $_sPair = $oRequest->query->get('pair');
        /** @var HistoryService $_oHistoryService */
        $_oHistoryService = $this->get(HistoryService::class);
        $_oSince = $_oHistoryService->getSince($_sPair);
        return new JsonResponse($_oSince?$_oSince->getTimestamp():null);
    }
}
