<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/2/17
 * Time: 11:16 PM
 */

namespace AppBundle\Controller;

use AppBundle\Service\PortfolioService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PortfolioController extends Controller
{
    /**
     * @Route("/portfolio", name="portfolio")
     * @return JsonResponse
     */
    public function portfolioAction()
    {
        /** @var PortfolioService $_oPortfolioService */
        $_oPortfolioService = $this->get(PortfolioService::class);
        $_aPortfolio = $_oPortfolioService->getPortfolio();

        return new JsonResponse($_aPortfolio);
    }
}
