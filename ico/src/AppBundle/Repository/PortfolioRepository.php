<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/2/17
 * Time: 11:33 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Portfolio;

class PortfolioRepository extends AbstractRepository
{
    /**
     * @param Portfolio $oPortfolio
     * @return Portfolio
     */
    public function savePortfolio($oPortfolio)
    {
        return $this->saveEntity($oPortfolio);
    }

    /**
     * @return Portfolio[]
     */
    public function getPortfolio()
    {
        $_sSql  = "SELECT";
        $_sSql .=  " ordertxid";
        $_sSql .=  ", pair";
        $_sSql .=  ", time";
        $_sSql .=  ", type";
        $_sSql .=  ", order_type AS ordertype";
        $_sSql .=  ", price";
        $_sSql .=  ", cost";
        $_sSql .=  ", fee";
        $_sSql .=  ", vol";
        $_sSql .=  ", margin";
        $_sSql .= " FROM " . Portfolio::TABLE_NAME;

        return Portfolio::buildPortfolios($this->fetchAll($_sSql));
    }

    public function deletePortfolio()
    {
        $_sSql  = "DELETE";
        $_sSql .= " FROM " . Portfolio::TABLE_NAME;
        $_sSql .= " WHERE 1";

        $this->executeQuery($_sSql);
    }
}