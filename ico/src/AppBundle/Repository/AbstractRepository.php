<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/1/17
 * Time: 12:22 PM
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{
    /**
     * @param $oEntity
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function saveEntity($oEntity)
    {
        $oEm = $this->getEntityManager();
        $oEm->merge($oEntity);
        $oEm->flush();

        return $oEntity;
    }

    /**
     * @param $sSql
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function fetch($sSql) {
        $_oQuery = $this->executeQuery($sSql);
        return $_oQuery->fetch();
    }

    /**
     * @param $sSql
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function fetchAll($sSql) {
        $_oQuery = $this->executeQuery($sSql);
        return $_oQuery->fetchAll();
    }

    /**
     * @param $sSql
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function executeQuery($sSql)
    {
        $_oQuery = $this->getEntityManager()->getConnection()->prepare($sSql);
        $_oQuery->execute();

        return $_oQuery;
    }
}
