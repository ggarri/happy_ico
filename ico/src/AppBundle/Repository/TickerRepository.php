<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 20/12/17
 * Time: 21:17
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Ticker;

class TickerRepository extends AbstractRepository
{
    /**
     * @param $oTicker
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveTicker($oTicker)
    {
        $this->saveEntity($oTicker);
    }

    /**
     * @param string $sPair
     * @param string|null $sSince
     * @return mixed
     */
    public function getTickers($sPair, $sSince = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Ticker::class, 'p')
            ->where('p.sPair = :pair')
            ->orderBy('p.iTime', 'DESC')
            ->setParameter('pair', $sPair);

        if($sSince) {
            $oTime = \DateTime::createFromFormat('YmdHi', $sSince);
            $qb->andWhere('p.iTime >= :time')
                ->setParameter('time', $oTime);
        }

        $query = $qb->getQuery();
        $results = $query->getResult();
        return $results;
    }

    /**
     * @param $aTicker
     * @return Ticker
     */
    public function buildTicker($aTicker)
    {
        $_oTicker = new Ticker();
        $_oTicker->setTime((new \DateTime())->setTimestamp($aTicker['time']));
        $_oTicker->setPair($aTicker['pair']);
        $_oTicker->setAsk(static::arrayFillKeys($aTicker['ask'], ['price', 'wholeLotVolume', 'lotVolume']));
        $_oTicker->setBid(static::arrayFillKeys($aTicker['bid'], ['price', 'wholeLotVolume', 'lotVolume']));
        $_oTicker->setLastTrade(static::arrayFillKeys($aTicker['lastTrade'], ['price', 'lotVolume']));
        $_oTicker->setVolume(static::arrayFillKeys($aTicker['volume'], ['today', 'last24Hours']));
        $_oTicker->setVolumeWeightedAverage(static::arrayFillKeys($aTicker['volumeWeightedAverage'], ['today', 'last24Hours']));
        $_oTicker->setNumberTrades(static::arrayFillKeys($aTicker['numberTrades'], ['today', 'last24Hours']));
        $_oTicker->setLow(static::arrayFillKeys($aTicker['low'], ['today', 'last24Hours']));
        $_oTicker->setHigh(static::arrayFillKeys($aTicker['high'], ['today', 'last24Hours']));
        $_oTicker->setTodayOpenPrice($aTicker['todayOpenPrice']);

        return $_oTicker;
    }

    static protected function arrayFillKeys($aArray, $aIndexes)
    {
        $aArray = array_map('floatval', json_decode($aArray));
        $aArray = array_combine($aIndexes , $aArray);
        return json_encode($aArray, JSON_UNESCAPED_UNICODE);
    }
}