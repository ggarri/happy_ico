<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/1/17
 * Time: 12:04 PM
 */

namespace AppBundle\Repository;

use AppBundle\Entity\LastOhlc;

class LastOhlcRepository extends AbstractRepository
{
    /**
     * @param $oLastOhlc
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveLastOhlc($oLastOhlc)
    {
        return $this->saveEntity($oLastOhlc);
    }

    /**
     * @param $sPair
     * @param $iInterval
     * @return int|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getLastTimeOhlcSaved($sPair, $iInterval)
    {
        $_sSql  = "SELECT";
        $_sSql .=  " pair";
        $_sSql .=  ", period";
        $_sSql .=  ", time";
        $_sSql .= " FROM " . LastOhlc::TABLE_NAME;
        $_sSql .= " WHERE pair = '" . $sPair . "'";
        $_sSql .= " AND period = " . (int) $iInterval;

        $_aLastOhlc = $this->fetch($_sSql);

        if (!$_aLastOhlc)
        {
            return null;
        }

        return $_aLastOhlc['time'];
    }
}