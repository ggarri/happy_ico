<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/21/17
 * Time: 3:32 PM
 */

namespace AppBundle\Repository;
use AppBundle\Entity\Ohlc;
use Doctrine\ORM\OptimisticLockException;

class OhlcRepository extends AbstractRepository
{
    /**
     * @param Ohlc[] $aOhlc
     * @throws OptimisticLockException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function saveOhlc($aOhlc)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();

        try
        {
            $batchSize = 200;
            foreach($aOhlc as $idx => $_oOhlc) {
                $this->persistOhlc($_oOhlc);

                if (($idx % $batchSize) === 0) {
                    $em->flush();
                    $em->clear(); // Detaches all objects from Doctrine!
                }
            }

            $em->commit();
        }
        catch (OptimisticLockException $_oE)
        {
            $em->getConnection()->rollback();
            throw $_oE;
        }
    }

    /**
     * @param string $sPair
     * @param int|null $iInterval
     * @param string|null $sSince
     * @return mixed
     */
    public function getOhlc($sPair, $iInterval = null, $sSince = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Ohlc::class, 'p')
            ->where('p.sPair = :pair')
            ->orderBy('p.iTime', 'DESC')
            ->setParameter('pair', $sPair);

        if($iInterval) {
            $qb->andWhere('p.iInterval = :interval')
                ->setParameter('interval', $iInterval);
        }

        if($sSince) {
            $time = \DateTime::createFromFormat('YmdHi', $sSince);
            $qb->andWhere('p.iTime >= :time')
                ->setParameter('time', $time);
        }
            
        $query = $qb->getQuery();
        $results = $query->getResult();
        return $results;
    }

    /**
     * @param $sPair
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSince($sPair)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('MIN(p.iTime)')
            ->from(Ohlc::class, 'p')
            ->where('p.sPair = :pair')
            ->setParameter('pair', $sPair);

        $query = $qb->getQuery();
        $results = $query->getOneOrNullResult(2);
        return $results;
    }
    
    public function getAvailablePairs()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p.sPair')
            ->from(Ohlc::class, 'p')
            ->groupBy('p.sPair');

        $query = $qb->getQuery();
        $results = $query->getResult(2);
        return $results;
    }

    /**
     * @param $aOhlc
     * @return array
     */
    public function buildOhlc($aOhlc)
    {
        $_aOhlcResult = [];
        foreach ($aOhlc as $_aOhlc)
        {
            $_oOhlc = new Ohlc();
            $_oOhlc->setTime((new \DateTime())->setTimestamp($_aOhlc['time']));
            $_oOhlc->setPair($_aOhlc['pair']);
            $_oOhlc->setInterval((int)$_aOhlc['interval']);
            $_oOhlc->setVolume(floatval($_aOhlc['volume']));
            $_oOhlc->setClose(floatval($_aOhlc['close']));
            $_oOhlc->setOpen(floatval($_aOhlc['open']));
            $_oOhlc->setHigh(floatval($_aOhlc['high']));
            $_oOhlc->setLow(floatval($_aOhlc['low']));
//            $_oOhlc->setAsks($_aPoint['asks']);
//            $_oOhlc->setBids($_aPoint['bids']);

            $_aOhlcResult[] = $_oOhlc;
        }

        return $_aOhlcResult;
    }

    /**
     * @param Ohlc $oOhlc
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function persistOhlc($oOhlc)
    {
        $_sSql  = "INSERT IGNORE INTO " . Ohlc::TABLE_NAME;
        $_sSql .= " (time, pair, period, low, high, open, close, volume)";
        $_sSql .= " VALUES(";
        $_sSql .= " '" . $oOhlc->getTime()->format('Y-m-d H:i:s') . "'";
        $_sSql .= ", '" . $oOhlc->getPair() . "'";
        $_sSql .= ", " . $oOhlc->getInterval();
        $_sSql .= ", " . $oOhlc->getLow();
        $_sSql .= ", " . $oOhlc->getHigh();
        $_sSql .= ", " . $oOhlc->getOpen();
        $_sSql .= ", " . $oOhlc->getClose();
        $_sSql .= ", " . $oOhlc->getVolume();
        $_sSql .= ")";

        $this->executeQuery($_sSql);
    }
}
