<?php

namespace AppBundle\Service;

use AppBundle\Component\Api\AbstractExchange;
use AppBundle\Entity\LastOhlc;
use AppBundle\Repository\OhlcRepository;
use AppBundle\Repository\LastOhlcRepository;
use AppBundle\Repository\TickerRepository;

/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/21/17
 * Time: 3:35 PM
 */
class HistoryService
{
    /** @var  OhlcRepository */
    protected $oOhlcRepository;

    /** @var  LastOhlcRepository */
    protected $oLastOhlcRepository;

    /** @var  TickerRepository */
    protected $oTickerRepository;

    /** @var  AbstractExchange */
    protected $oTradeEngine;

    /**
     * HistoryService constructor.
     * @param OhlcRepository $oOhlcRepository
     * @param LastOhlcRepository $oLastOhlcRepository
     * @param TickerRepository $oTickerRepository
     * @param AbstractExchange $oTradeEngine
     */
    public function __construct($oOhlcRepository, $oLastOhlcRepository, $oTickerRepository, $oTradeEngine)
    {
        $this->oOhlcRepository = $oOhlcRepository;
        $this->oLastOhlcRepository = $oLastOhlcRepository;
        $this->oTickerRepository = $oTickerRepository;
        $this->oTradeEngine = $oTradeEngine;
    }

    /**
     * @param $sPair
     * @param null $iInterval
     * @param null $sSince
     * @return array
     */
    public function getOhlc($sPair, $iInterval = null, $sSince = null)
    {
        $_oHistory = $this->oOhlcRepository->getOhlc($sPair, $iInterval, $sSince);
        foreach($_oHistory as &$point) {
            $point->setTime($point->getTime()->getTimestamp());
        }
        return $_oHistory;
    }

    /**
     * @param $sPair
     * @param int|null $sSince
     * @return array
     */
    public function getTicker($sPair, $sSince = null)
    {
        $_oHistory = $this->oTickerRepository->getTickers($sPair, $sSince);
        foreach($_oHistory as &$point) {
            $point->setTime($point->getTime()->getTimestamp());
        }
        return $_oHistory;
    }

    public function getAvailablePairs()
    {
        return array_map(function($oPair) {
            return $oPair['sPair'];
        }, $this->oOhlcRepository->getAvailablePairs());
    }

    /**
     * @param $sPair
     * @return \DateTime|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSince($sPair) {
        $oRow = $this->oOhlcRepository->getSince($sPair);
        return $oRow ? new \DateTime(reset($oRow)) : null;
    }

    /**
     * @param $sPair
     * @param $iInterval
     * @param $fIntervalAskBid
     * @param null $iSince
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function importPastHistory($sPair, $iInterval, $fIntervalAskBid, $iSince = null)
    {
        $this->oTradeEngine->init($sPair, $iInterval, $fIntervalAskBid, $iSince);
        $_aOhlc = $this->oTradeEngine->getOhlc();
        $_aOhlc = $this->oOhlcRepository->buildOhlc($_aOhlc);
        $this->oOhlcRepository->saveOhlc($_aOhlc);
    }

    /**
     * @param $sPair
     * @param $iInterval
     * @param $fIntervalAskBid
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function importCurrentHistory($sPair, $iInterval, $fIntervalAskBid)
    {
        $_iLastTimeOhlcSaved = $this->oLastOhlcRepository->getLastTimeOhlcSaved($sPair, $iInterval);
        $this->oTradeEngine->init($sPair, $iInterval, $fIntervalAskBid, $_iLastTimeOhlcSaved);

        $_aOhlc = $this->oTradeEngine->getOhlc();
        $_aOhlc = $this->oOhlcRepository->buildOhlc($_aOhlc);
        $this->oOhlcRepository->saveOhlc($_aOhlc);

        $_aTicker = $this->oTradeEngine->getTicker();
        $_oTicker = $this->oTickerRepository->buildTicker($_aTicker);
        $this->oTickerRepository->saveTicker($_oTicker);

        $_oLastOhlc = new LastOhlc();
        $_oLastOhlc->setPair($sPair);
        $_oLastOhlc->setInterval($iInterval);
        $_oLastOhlc->setTime($this->oTradeEngine->getLastTimeOhlc());
        $this->oLastOhlcRepository->saveLastOhlc($_oLastOhlc);
    }

    public function getIntervals()
    {
        return $this->oTradeEngine->getIntervals();
    }
}
