<?php
/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/2/17
 * Time: 11:20 PM
 */

namespace AppBundle\Service;

use AppBundle\Component\Api\AbstractExchange;
use AppBundle\Entity\Portfolio;
use AppBundle\Repository\PortfolioRepository;

class PortfolioService
{
    /** @var  PortfolioRepository */
    protected $oPortfolioRepository;

    /** @var  AbstractExchange */
    protected $oTradeEngine;

    /**
     * PortfolioService constructor.
     * @param PortfolioRepository $oPortfolioRepository
     * @param AbstractExchange $oTradeEngine
     */
    public function __construct(PortfolioRepository $oPortfolioRepository, AbstractExchange $oTradeEngine)
    {
        $this->oPortfolioRepository = $oPortfolioRepository;
        $this->oTradeEngine = $oTradeEngine;
    }

    /**
     * @return Portfolio[]
     */
    public function getPortfolio()
    {
        $_aPortfolio = $this->oPortfolioRepository->getPortfolio();
        return Portfolio::portfoliosToArray($_aPortfolio);
    }

    public function importPortfolio()
    {
        $this->oPortfolioRepository->deletePortfolio();
        $_aPortfolios = $this->oTradeEngine->getPortfolio();

        foreach ($_aPortfolios as $_aPortfolio)
        {
            $this->oPortfolioRepository->savePortfolio(Portfolio::buildPortfolio($_aPortfolio));
        }
    }
}