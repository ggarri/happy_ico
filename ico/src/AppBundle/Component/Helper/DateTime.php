<?php

namespace AppBundle\Component\Helper;

/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 10/22/17
 * Time: 8:06 PM
 */
class DateTime
{
    const HUMAN_FORMAT = 'Y/m/d H:i:s';
    const COMMAND_FORMAT = 'YmdHis';

    static public function timeCommandToTimestamp($sDateTime)
    {
        if (is_null($sDateTime))
        {
            return null;
        }

        $_oDate = \DateTime::createFromFormat(static::COMMAND_FORMAT, $sDateTime);
        return $_oDate->getTimestamp();
    }

    static public function timestampToHumanTime($iTimestamp)
    {
        $_sSince = new \DateTime();
        $_sSince = $_sSince->setTimestamp($iTimestamp);
        return $_sSince->format(DateTime::HUMAN_FORMAT);
    }
}
