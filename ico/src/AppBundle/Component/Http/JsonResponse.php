<?php

namespace AppBundle\Component\Http;

use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializerBuilder;

class JsonResponse extends Response
{
    static protected $aHeaders = [
        'Content-Type' => 'application/json',
    ];

    public function __construct($content = [], $status = 200, array $headers = array())
    {
        $_oSerializer = SerializerBuilder::create()->build();
        $_oContext = new SerializationContext();
        $_oContext->setSerializeNull(true);

        parent::__construct(
            $_oSerializer->serialize($content, 'json', $_oContext),
            $status,
            self::$aHeaders
        );
    }
}
