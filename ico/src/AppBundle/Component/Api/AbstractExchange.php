<?php

namespace AppBundle\Component\Api;

use AppBundle\Component\Api\Helper\AskBid;

abstract class AbstractExchange
{
    const NUMBER_RETRIES = 10;

    /** @var ApiConnector  */
    protected $oApi;

    /** @var string */
    protected $sPair;

    /** @var int */
    protected $iInterval;

    /** @var int */
    protected $iSince;

    /** @var  array */
    protected $aOhlc;

    /** @var  array */
    protected $aTicker;

    /** @var  array */
    protected $aAsks;

    /** @var  array */
    protected $aBids;

    /** @var  float */
    protected $fIntervalAskBid;

    /** @var  double */
    protected $fLastTimeAskBid;

    /** @var  double */
    protected $fLastTimeOhlc;

    /**
     * AbstractExchange constructor.
     * @param $key
     * @param $secret
     * @param string $url
     * @param string $version
     * @param bool $sslverify
     */
    public function __construct($key, $secret, $url , $version, $sslverify)
    {
        $this->oApi = new ApiConnector($key, $secret, $url, $version, $sslverify);
    }

    /**
     * @param string $sPair
     * @param int $iInterval
     * @param int $fIntervalAskBid
     * @param int $iSince
     */
    public function init($sPair, $iInterval = 1, $fIntervalAskBid = 1, $iSince = null)
    {
        $this->aOhlc = [];
        $this->aAsks = [];
        $this->aBids = [];

        $this->sPair = $sPair;
        $this->fIntervalAskBid = $fIntervalAskBid;
        $this->iInterval = $iInterval;
        $this->iSince = $iSince;
    }

    abstract protected function buildOhlc();

    abstract public function buildDepth();

    /** @param int $iSince */
    abstract public function buildTrade($iSince = null);

    abstract protected function buildTicker();

    /** @return array */
    abstract public function getPortfolio();

    /** @return array */
    abstract public function getIntervals();

    /** @return array */
    public function getOhlc()
    {
        $this->buildOhlc();
        return $this->aOhlc;
    }

    /** @return array */
    public function getTicker()
    {
        $this->buildTicker();
        return $this->aTicker;
    }

    /** @return float */
    public function getLastTimeAskBid()
    {
        return $this->fLastTimeAskBid;
    }

    /** @return float */
    public function getLastTimeOhlc()
    {
        return $this->fLastTimeOhlc;
    }

    public function mergeAsksBids()
    {
        foreach ($this->aOhlc as $_iKey => $_aPoint)
        {
            $_aAskResult = AskBid::getAsksBidsForPoint($_aPoint, $this->aAsks, $this->iInterval, $this->fIntervalAskBid);
            $_aBidResult = AskBid::getAsksBidsForPoint($_aPoint, $this->aBids, $this->iInterval, $this->fIntervalAskBid, false);
            $this->aOhlc[$_iKey]['bids'] = json_encode($_aBidResult, JSON_FORCE_OBJECT);
            $this->aOhlc[$_iKey]['asks'] = json_encode($_aAskResult, JSON_FORCE_OBJECT);
        }

        $this->resetAskBid();
    }

    /** @return bool */
    public function needMoreAskBid()
    {
        if (empty($this->aOhlc))
        {
            return false;
        }

        if (is_null($this->fLastTimeOhlc) || is_null($this->fLastTimeAskBid))
        {
            return true;
        }

        return static::getTimestampMicroSec($this->fLastTimeOhlc) > static::getTimestampMicroSec($this->fLastTimeAskBid);
    }

    protected function resetAskBid()
    {
        $this->aAsks = [];
        $this->aBids = [];
    }

    static public function getTimestampMicroSec($iTimestamp)
    {
        return str_pad($iTimestamp, 19, '0');
    }
}
