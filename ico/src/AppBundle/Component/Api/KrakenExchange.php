<?php

namespace AppBundle\Component\Api;

class KrakenExchange extends AbstractExchange
{
    static $sBaseUrl = "https://api.kraken.com/";
    static $sVersion = "0";
    static $sPublicOhlcMethod = "OHLC";
    static $sPublicDepthMethod = "Depth";
    static $sPublicTradeMethod = "Trades";
    static $sPublicTickerMethod = "Ticker";
    static $sPublicTimeMethod = "Time";
    static $sPrivateTradesHistoryMethod = "TradesHistory";
    static $iMaxCountDepth = 500;

    /**
     * @return array|mixed
     * @throws ApiException
     */
    public function getPortfolio()
    {
        $_aPorfolio = $this->getDataFromRequest(static::$sPrivateTradesHistoryMethod, false, []);
        return $_aPorfolio['trades'];
    }

    /**
     * @return array
     */
    public function getIntervals()
    {
        return [1, 5, 15, 30, 60, 240, 1440, 10080, 21600];
    }

    /**
     * @param null $iSince
     * @throws ApiException
     */
    public function buildTrade($iSince = null)
    {
        $_aAsksAndBids = $this->getDataFromRequest(static::$sPublicTradeMethod, true, [
            'pair' => $this->sPair,
            'since' . $this->iSince
        ]);

        $this->fLastTimeAskBid = isset($_aAsksAndBids['last']) ? $_aAsksAndBids['last'] : null;
        $this->parseTrade($_aAsksAndBids[static::getPairIndex($this->sPair)]);
    }

    /**
     * @throws ApiException
     */
    public function buildDepth()
    {
        $_aAsksAndBids = $this->getDataFromRequest(static::$sPublicDepthMethod, true, [
            'pair' => $this->sPair,
            'count=' . static::$iMaxCountDepth
        ]);

        $this->fLastTimeAskBid = isset($_aAsksAndBids['last']) ? $_aAsksAndBids['last'] : null;
        $this->parseDepth($_aAsksAndBids[static::getPairIndex($this->sPair)]);
    }

    /**
     * @throws ApiException
     */
    protected function buildOhlc()
    {
        $this->aOhlc = [];
        $_aHistory = $this->getDataFromRequest(static::$sPublicOhlcMethod, true, [
            'pair' => $this->sPair,
            'interval' => $this->iInterval,
            'since' . $this->iSince
        ]);

        foreach ($_aHistory[static::getPairIndex($this->sPair)] as $_aPoint)
        {
            $_aPointParsed = [];
            $_aPointParsed['pair'] = $this->sPair;
            $_aPointParsed['interval'] = $this->iInterval;
            $_aPointParsed['time'] = $_aPoint[0];
            $_aPointParsed['open'] = $_aPoint[1];
            $_aPointParsed['close'] = $_aPoint[4];
            $_aPointParsed['low'] = $_aPoint[3];
            $_aPointParsed['high'] = $_aPoint[2];
            $_aPointParsed['volume'] = $_aPoint[6];
            $_aPointParsed['bids'] = [];
            $_aPointParsed['asks'] = [];

            $this->aOhlc[] = $_aPointParsed;
        }

        $this->fLastTimeOhlc = isset($_aHistory['last']) ? $_aHistory['last'] : null;
    }

    /**
     * @throws ApiException
     */
    protected function buildTicker()
    {
        $_aTicker = $this->getDataFromRequest(static::$sPublicTickerMethod, true, [
            'pair' => $this->sPair
        ]);
        $_aTicker = $_aTicker[static::getPairIndex($this->sPair)];

        $_aTimestamp = $_aHistory = $this->getDataFromRequest(static::$sPublicTimeMethod, true, [
            'pair' => $this->sPair
        ]);

        $_aTickerParsed = [];
        $_aTickerParsed['pair'] = $this->sPair;
        $_aTickerParsed['interval'] = $this->iInterval;
        $_aTickerParsed['time'] = $_aTimestamp['unixtime'];
        $_aTickerParsed['bid'] = json_encode($_aTicker['b']);
        $_aTickerParsed['ask'] = json_encode($_aTicker['a']);
        $_aTickerParsed['lastTrade'] = json_encode($_aTicker['c']);
        $_aTickerParsed['volume'] = json_encode($_aTicker['v']);
        $_aTickerParsed['volumeWeightedAverage'] = json_encode($_aTicker['p']);
        $_aTickerParsed['numberTrades'] = json_encode($_aTicker['t']);
        $_aTickerParsed['low'] = json_encode($_aTicker['l']);
        $_aTickerParsed['high'] = json_encode($_aTicker['h']);
        $_aTickerParsed['todayOpenPrice'] = $_aTicker['o'];

        $this->aTicker = $_aTickerParsed;
    }

    /**
     * @param $sMethod
     * @param bool $bPublic
     * @param array $aOptions
     * @return mixed
     * @throws ApiException
     */
    protected function getDataFromRequest($sMethod, $bPublic = true, $aOptions = [])
    {
        $_iCount = 1;
        do
        {
            $_aResponse = $bPublic
                ? $this->oApi->queryPublic($sMethod, $aOptions)
                : $this->oApi->queryPrivate($sMethod, $aOptions);

            if (isset($_aResponse['error']) && !empty($_aResponse['error']))
            {
                print_r($_aResponse['error']);
            }

            $_iCount++;
        } while (!isset($_aResponse["result"]) && ($_iCount <= static::NUMBER_RETRIES));

        return $_aResponse["result"];
    }

    protected function parseDepth($aAskBid)
    {
        foreach ($aAskBid['asks'] as $_aAsk)
        {
            $this->aAsks[] = $_aAsk;
        }

        foreach ($aAskBid['bids'] as $_aBid)
        {
            $this->aBids[] = $_aBid;
        }
    }

    protected function parseTrade($aAskBid)
    {
        foreach ($aAskBid as $_aTrade)
        {
            if ($_aTrade[3] == 's')
            {
                $this->aAsks[] = $_aTrade;
            }
            else
            {
                $this->aBids[] = $_aTrade;;
            }
        }
    }

    static protected function getPairIndex($sPair)
    {
        if($sPair=='BTCEUR') return 'XXBTZEUR';
        if($sPair=='BCHEUR') return 'BCHEUR';
        return substr_replace('X' . $sPair, 'Z', 4, 0);
    }
}
