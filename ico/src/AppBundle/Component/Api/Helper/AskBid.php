<?php

/**
 * Created by PhpStorm.
 * User: jlopez
 * Date: 11/1/17
 * Time: 6:46 PM
 */

namespace AppBundle\Component\Api\Helper;

class AskBid
{
    /**
     * @param array $aPoint
     * @param $aAskOrBid
     * @param $iInterval
     * @param $fIntervalAskBid
     * @param $bAsks
     * @return array
     */
    static public function getAsksBidsForPoint($aPoint, $aAskOrBid, $iInterval, $fIntervalAskBid, $bAsks = true)
    {
        $_iIntervalSec = $iInterval * 60;
        $_fCurrentPrice = $aPoint['close'];
        $_aMetricGrouped = [];

        foreach ($aAskOrBid as $_aMetric)
        {
            $_fPriceOperate = $_aMetric[0];
            $_fVolume = $_aMetric[1];
            $_iDatetime = $_aMetric[2];

            if (($_iDatetime >= $aPoint['time']) && ($_iDatetime < ($aPoint['time'] + $_iIntervalSec)))
            {
                $_fInterval = static::getNumIntervalFromCurrentPrice($_fCurrentPrice, $_fPriceOperate, $fIntervalAskBid, $bAsks);

                $_aMetricGrouped[$_fInterval] = isset($_aMetricGrouped[$_fInterval])
                    ? $_aMetricGrouped[$_fInterval] + $_fVolume
                    : $_fVolume;
            }
        }

        return $_aMetricGrouped;
    }

    static protected function getNumIntervalFromCurrentPrice($fCurrentPrice, $fPrice, $fIntervalAskBid, $bAsk)
    {
        $fCurrentPrice = floatval($fCurrentPrice);
        $fPrice = floatval($fPrice);

        $_fCurrentPriceCutted = static::scaleNumberByIntervalAskBid($fCurrentPrice, $fIntervalAskBid);
        $_fPriceCutted = static::scaleNumberByIntervalAskBid($fPrice, $fIntervalAskBid);

        if (($fCurrentPrice < $fPrice) && $bAsk)
        {
            $_fNumInterval = $fIntervalAskBid;
            $_fStartLoop = $_fCurrentPriceCutted;
            $_fCondLoop = $_fPriceCutted;
            $_fSumLoop = 1;
        }
        elseif (($fCurrentPrice < $fPrice) && !$bAsk)
        {
            $_fNumInterval = -$fIntervalAskBid;
            $_fStartLoop = $_fPriceCutted;
            $_fCondLoop = $_fCurrentPriceCutted;
            $_fSumLoop = -1;
        }
        elseif (($fCurrentPrice > $fPrice) && $bAsk)
        {
            $_fNumInterval = -$fIntervalAskBid;
            $_fStartLoop = $_fCurrentPriceCutted;
            $_fCondLoop = $_fPriceCutted;
            $_fSumLoop = -1;

        }
        elseif (($fCurrentPrice > $fPrice) && !$bAsk)
        {
            $_fNumInterval = ($_fPriceCutted == $_fCurrentPriceCutted) ? $fIntervalAskBid : 0;
            $_fStartLoop = $_fPriceCutted;
            $_fCondLoop = $_fCurrentPriceCutted;
            $_fSumLoop = 1;
        }
        else
        {
            $_fNumInterval = 0;
            $_fStartLoop = 0;
            $_fCondLoop = 0;
            $_fSumLoop = 0;
        }

        for($_iI = (int)$_fStartLoop; $_iI < (int)$_fCondLoop; $_iI = $_iI + $_fSumLoop)
        {
            $_fNumInterval += $fIntervalAskBid;
        }

        return $_fNumInterval;
    }

    static protected function scaleNumberByIntervalAskBid($fNumber, $fIntervalAskBid)
    {
        return floor($fNumber / $fIntervalAskBid);
    }
}