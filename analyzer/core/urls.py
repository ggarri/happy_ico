from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'get-fake-data$', views.get_fake_data, name='get-fake-data'),
    url(r'get-data$', views.get_data, name='get-data'),
    url(r'get-trades$', views.get_trades, name='get-trades'),
    url(r'get-trade-trxs$', views.get_trade_trxs, name='get-trade-trxs'),
    url(r'get-available-pairs$', views.get_available_pairs, name='get-available-pairs'),
    url(r'get-available-periods$', views.get_available_periods, name='get-available-periods'),
    url(r'play-simulation$', views.play_simulation, name='play-simulation'),
]