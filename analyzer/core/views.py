import simplejson
import json

from collections import OrderedDict
from django.http import HttpResponse, JsonResponse

from .models.portfolio import Portfolio

from .domain.tradeDomain import TradeDomain
from .domain.dataDomain import DataDomain

from .helpers.date import Date
from .helpers.serializer import to_serializable


# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def get_fake_data(request):
    pair = request.GET['pair']
    start = request.GET['start']
    freq = request.GET['freq']
    data = DataDomain.get_fake_data(pair, start, freq)
    return JsonResponse(data, safe=False)

def get_data(request):
    pair = request.GET['pair']
    since = Date.create(request.GET['since'], Date.api_format) if 'since' in request.GET else None
    interval = int(request.GET['interval']) if 'interval' in request.GET else 60
    data = DataDomain.get_data(pair, interval, since)
    return JsonResponse(data, safe=False)

def get_available_pairs(request):
    data = DataDomain.get_available_pairs()
    return JsonResponse(data, safe=False)

def get_available_periods(request):
    pair = request.GET['pair']
    interval = int(request.GET['interval']) if 'interval' in request.GET else 60
    startFrom, endTo = DataDomain.get_available_periods(pair, interval)
    return JsonResponse({
        'start': startFrom,
        'end': endTo,
        'interval': interval
    }, safe=False)

# def get_trades(request):
#     now = Date.create(request.GET['now'] if 'now' in request.GET else None, Date.api_format)
#     since = Date.create(request.GET['since'], Date.api_format) if 'since' in request.GET else None
#     pairs = [request.GET['pair']] if 'pair' in request.GET else None
#     pairTrades = TradeDomain.get_trades(pairs, now, since)
#     return HttpResponse(simplejson.dumps(pairTrades, ignore_nan=True), content_type='application/json')

def get_trades(request):
    now = Date.create(request.GET['now'] if 'now' in request.GET else None, Date.api_format)
    since = Date.create(request.GET['since'], Date.api_format) if 'since' in request.GET else None
    pairs = [request.GET['pair']] if 'pair' in request.GET else None

    trades, next_portfolio = TradeDomain.get_trades(None, pairs, now, since)
    return HttpResponse(simplejson.dumps(trades, ignore_nan=True, default=to_serializable), content_type='application/json')


def get_trade_trxs(request):
    now = Date.create(request.GET['now'] if 'now' in request.GET else None, Date.api_format)
    since = Date.create(request.GET['since'], Date.api_format) if 'since' in request.GET else None
    pairs = [request.GET['pair']] if 'pair' in request.GET else None

    portfolio = Portfolio(user_id=1, wallet={
        'EUR': 400,
        'ETH': 1.2,
    }, exchange_currency='EUR')

    trxs, next_portfolio = TradeDomain.get_trades(portfolio, pairs, now, since)
    return HttpResponse(simplejson.dumps(trxs, ignore_nan=True, default=to_serializable), content_type='application/json')

def play_simulation(request):
    now = Date.create(request.GET['now'] if 'now' in request.GET else None, Date.api_format)
    start_from = Date.create(request.GET['from'], Date.api_format)
    since = Date.create(request.GET['since'], Date.api_format) if 'since' in request.GET else None
    pair = request.GET['pair'] if 'pair' in request.GET else None
    freq = int(request.GET['freq']) if 'freq' in request.GET else 60

    # @TODO Read it from request
    if 'portfolio' in request.GET:
        portfolio = json.loads(request.GET['portfolio'])
        portfolio = Portfolio(user_id=portfolio['user_id'],
                              wallet=portfolio['wallet'],
                              exchange_currency=portfolio['exchange_currency']
                              )
    else:
        portfolio = Portfolio(user_id=1, wallet={
            'EUR': 400,
            'ETH': 1.2,
        }, exchange_currency='EUR')

    trxs, orig_portfolio, next_portfolio, candidate_portfolio = \
        TradeDomain.play_simulator(portfolio, [pair], freq, start_from, now, since)
    pairs = list(set(orig_portfolio.get_pairs() + next_portfolio.get_pairs()))
    orig_rates = TradeDomain.get_rates(pairs, start_from)
    next_rates = TradeDomain.get_rates(pairs, now)
    response = OrderedDict()
    response['orig_portfolio'] = orig_portfolio
    response['next_portfolio'] = next_portfolio
    response['candidate_portfolio'] = candidate_portfolio
    response['orig_rates'] = orig_rates
    response['next_rates'] = next_rates
    response['trxs'] = trxs
    return HttpResponse(simplejson.dumps(response, ignore_nan=True), content_type='application/json')