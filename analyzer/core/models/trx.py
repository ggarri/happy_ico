import time
import math

from ..helpers.date import Date

class Trx:
    ACTION_SELL = 'SELL'
    ACTION_BUY = 'BUY'

    PER_FEE = 0.0025

    pair = None
    values = None
    volume = None
    rate = None
    action = None
    fee = None
    timestamp = None

    def __init__(self, timestamp=time.time()):
        self.timestamp = timestamp

    @staticmethod
    def create_template(pair, action, rate, timestamp=time.time()):
        self = Trx(timestamp)
        self.pair = pair
        self.rate = rate
        self.action = action
        self.fee = 0.0
        return self

    def set_volume_appending_fee(self, volume):
        self.volume = volume
        self.fee = volume * self.rate * Trx.PER_FEE

    def _asdict(self):
        self.datetime = Date.format(Date.timestamp_to_date(self.timestamp), Date.default_format)
        return self.__dict__

    def __str__(self):
        return """
            Pair: %s
            Volume: %s
            Rate: %s
            Action: %s
            Fee: %s
            Timestamp: %s
        """ % (self.pair, self.volume,self.rate, self.action, self.fee, Date.timestamp_to_date(self.timestamp))