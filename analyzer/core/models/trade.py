from .strategyOutcome import StrategyOutcome

from ..models.trx import Trx

def mycmp(t1, t2):
    """
    :param Trade t1:
    :param Trade t2:
    :rtype int: [-1,0,-1]
    """
    action1 = t1.get_trx_action()
    action2 = t2.get_trx_action()
    if action1 != action2: return 1 if action1 == Trx.ACTION_SELL else -1

    margin1 = t1.get_trade_margin(action1)
    margin2 = t2.get_trade_margin(action2)
    return 1 if margin1 > margin2 else -1

class Trade():
    strategy_outcomes = None # type: dict[str, StrategyOutcome]
    trx = None # type: Trx
    pair = None # type: str

    def __init__(self, pair):
        self.pair = pair
        self.trx = None
        self.strategy_outcomes = {}

    def append_strategy_variables(self, name, outcome):
        self.strategy_outcomes[name] = outcome

    def set_trx(self, trx):
        self.trx = trx

    def get_final_strategy_variables(self):
        """
        :rtype: StrategyOutcome
        """
        if len(self.strategy_outcomes) == 1:
            outcomes = list(self.strategy_outcomes.values())
            return outcomes[0]
        # Implement it using outcome weights
        raise Exception('TODO get_final_strategy_variables')

    def get_trx_action(self):
        final_variables = self.get_final_strategy_variables()
        return Trx.ACTION_BUY if final_variables.up_odds > final_variables.down_odds else Trx.ACTION_SELL

    def get_trade_margin(self, action):
        final_variables = self.get_final_strategy_variables()
        if action == Trx.ACTION_BUY: return final_variables.profit_per * final_variables.up_odds
        elif action == Trx.ACTION_SELL: return final_variables.lost_per * final_variables.down_odds
        else: raise Exception('Invalid Action')

    # def get_latest_price(self):
    #     metrics = self.pair_metrics.calculate_ohlc()
    #     return metrics.close

    def __lt__(self, other):
        return mycmp(self, other) < 0

    def __gt__(self, other):
        return mycmp(self, other) > 0

    def __eq__(self, other):
        return mycmp(self, other) == 0

    def __le__(self, other):
        return mycmp(self, other) <= 0

    def __ge__(self, other):
        return mycmp(self, other) >= 0

    def __ne__(self, other):
        return mycmp(self, other) != 0

    def _asdict(self):
        self.action = self.get_trx_action()
        self.trade_margin = self.get_trade_margin(self.action)
        return self.__dict__

    def __str__(self):
        return """
            pair: %s
            strategy_outcome: %s
        """ % (self.pair, str(self.strategy_outcomes))