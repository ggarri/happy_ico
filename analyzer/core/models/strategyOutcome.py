from .trx import Trx

class StrategyOutcome():
    profit_per = None # type: float Possible profit percentage
    lost_per = None # type: float Possible lost percentage
    up_odds = None # type: float [0,1] Define the up_odds of achieve profit
    down_odds = None # type: float [0,1] Define the up_odds of fall in lost
    weight = None

    def __init__(self, pair, profit_per, lost_per, up_odds, down_odds):
        self.pair = pair
        self.profit_per = profit_per
        self.lost_per = lost_per
        self.up_odds = up_odds
        self.down_odds = down_odds

    def set_weight(self, weight):
        self.weight = weight


    def _asdict(self):
        return self.__dict__

    def __str__(self):
        return """
            pair: %s
            profit_per: %f
            lost_per: %f
            down_odds: %f
            up_odds: %f
        """ % (self.pair,self.profit_per, self.lost_per, self.down_odds, self.up_odds)