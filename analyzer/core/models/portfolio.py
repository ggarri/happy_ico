import json
import math
import copy

from .trx import Trx
from ..helpers.date import Date

class Portfolio:
    """
    :type wallet: dict
    """
    wallet = None
    user_id = None
    exchange_currency = 'EUR'
    total_in_exchange_currency = None

    def __init__(self, user_id, wallet, exchange_currency = 'EUR'):
        self.user_id = user_id
        self.wallet = wallet
        if exchange_currency not in wallet: self.wallet[exchange_currency] = 1000
        self.exchange_currency = exchange_currency

    def get_pairs(self):
        return [ currency + self.exchange_currency for currency in self.wallet if currency != self.exchange_currency ]

    def get_total_in_exchange_currency(self, rates):
        op_currency = self.exchange_currency
        total_in_op_currency = 0.0
        for currency, volume in self.wallet.items():
            if currency == op_currency: total_in_op_currency += (0.0 if math.isnan(volume) else volume)
            else:
                pair = (currency + op_currency)
                total_in_op_currency += (rates[pair]*volume if pair in rates and not math.isnan(rates[pair]) else 0.0)
        return total_in_op_currency

    def get_wallet_pair(self, pair):
        return self.get_wallet_currency(Portfolio.extract_op_currency(pair))

    def get_wallet_currency(self, currency):
        return self.wallet[currency] if currency in self.wallet else 0

    def get_exchange_currency(self):
        return self.exchange_currency

    def set_total_in_exchange_currency(self, rates):
        self.total_in_exchange_currency = self.get_total_in_exchange_currency(rates)

    @staticmethod
    def clone(portfolio):
        return copy.deepcopy(portfolio)

    @staticmethod
    def create_from_json(json_portfolio):
        data = json.loads(json_portfolio)
        portfolio = Portfolio(data['user'], data['wallet'])
        # portfolio = Portfolio(1, {
        #     'EUR': 1000,
        #     'ETH': 2,
        #     'XRP': 3
        # })
        return portfolio

    @staticmethod
    def extract_op_currency(pair):
        return pair[0:3]

    @staticmethod
    def apply_trx(portfolio, trx, now = None):
        """
        :type next_portfolio: Portfolio
        :rtype : Portfolio
        """
        next_portfolio = Portfolio.clone(portfolio)
        exchange_currency = next_portfolio.exchange_currency
        price = trx.volume * trx.rate
        op_currency = Portfolio.extract_op_currency(trx.pair)
        next_portfolio.wallet[exchange_currency] -= trx.fee
        if op_currency not in portfolio.wallet: next_portfolio.wallet[op_currency] = 0.0

        if trx.action == Trx.ACTION_SELL:
            next_portfolio.wallet[exchange_currency] += price
            next_portfolio.wallet[op_currency] -= trx.volume

        elif trx.action == Trx.ACTION_BUY:
            next_portfolio.wallet[exchange_currency] -= price
            next_portfolio.wallet[op_currency] += trx.volume

        trx.timestamp = Date.date_to_timestamp(Date.create() if now is None else now)
        if next_portfolio.wallet[exchange_currency] < 0.0: raise ValueError('Invalid exchange currency amount')
        if next_portfolio.wallet[op_currency] < 0.0: raise ValueError('Invalid op currency amount')

        return next_portfolio

    def _asdict(self):
        return self.__dict__

    def __str__(self):
        return """
        UserId: %s
        Wallet: %s
        """ % (self.user_id, self.wallet)