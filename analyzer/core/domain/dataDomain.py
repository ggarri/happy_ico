from datetime import datetime

from ..services.icoConnector import IcoConnector
from ..services.statProviderService import StatProviderService

from ..helpers.date import Date

class DataDomain:
    @staticmethod
    def get_data(pair, interval, since = None):
        """
        :param str pair:
        :param datetime since:
        :rtype list:
        """
        data = IcoConnector.get_data(pair, since)
        if len(data) == 0: raise ValueError('There is not data for selected date %s' % Date.format(since))
        stat_provider = StatProviderService(pair, data)
        min_ts = stat_provider.get_min('time')
        min_interval = stat_provider.get_min_where('interval', 'time == %d' % min_ts)
        if min_interval > interval:
            raise ValueError('Min Date for requested date %s is: %d' % (Date.format(Date.timestamp_to_date(min_ts)), min_interval))
        agg_data = stat_provider.ohlc(interval)
        return agg_data.to_dict('records')

    @staticmethod
    def get_available_pairs():
        """
        :param str pair:
        :param datetime since:
        :rtype list:
        """
        return IcoConnector.get_available_pairs()

    @staticmethod
    def get_available_periods(pair, interval=60):
        """
        :param str pair:
        :rtype list:
        """
        data = IcoConnector.get_data(pair)
        stat_provider = StatProviderService(pair, data)
        min_ts = stat_provider.get_min_where('time', 'interval <= %d' % interval)
        max_ts = stat_provider.get_max_where('time', 'interval <= %d' % interval)
        return Date.timestamp_to_date(min_ts), Date.timestamp_to_date(max_ts)

    @staticmethod
    def get_fake_data(pair, since, freq):
        """
        :param str pair:
        :param datetime since:
        :param str freq: ['D', 'H']
        :rtype list:
        """
        return IcoConnector.get_fake_data(pair, since, freq)