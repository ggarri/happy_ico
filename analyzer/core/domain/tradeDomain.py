import time
import pandas as pd

from ..services.icoConnector import IcoConnector
from ..services.statProviderService import StatProviderService
from ..services.brokerService import BrokerService

from ..strategy.basicDayTrading import BasicDayTrading
from ..strategy.stopLoseTrading import StopLostTrading

from ..models.portfolio import Portfolio
from ..helpers.date import Date


class TradeDomain:
    @staticmethod
    def get_trades(portfolio=None, pairs=None, now=None, since=None):
        if now is None: now = Date.create()
        simulated_pairs = pairs if pairs is not None else IcoConnector.get_available_pairs()
        strategies = [
            [BasicDayTrading(), 1.0]
        ]

        trades = []
        for pair in simulated_pairs:
            data = IcoConnector.get_data(pair, since)
            stat_service = StatProviderService(pair, data, now)
            trade = BrokerService.calculate_pair_trade(strategies, stat_service)
            trades.append(trade)

        trades.sort()
        next_portfolio = Portfolio.clone(portfolio) if portfolio is not None else None
        for trade in trades:
            if trade.trx is None: continue
            if portfolio is None: continue
            volume = BrokerService.calculate_trx_volume(trade.trx, next_portfolio)
            if volume == 0: trade.set_trx(None); continue

            trade.trx.set_volume_appending_fee(volume)
            try: next_portfolio = Portfolio.apply_trx(next_portfolio, trade.trx, now)
            except ValueError: trade.set_trx(None)

        return trades, next_portfolio


    @staticmethod
    def play_simulator(portfolio, pairs, freq, start_from, now = None, since = None):
        if now is None: now = Date.create()
        simulated_pairs = pairs if pairs is not None else IcoConnector.get_available_pairs()

        trx_list = []
        from_rates = TradeDomain.get_rates(simulated_pairs, start_from)
        now_rates = TradeDomain.get_rates(simulated_pairs, now)

        next_portfolio = Portfolio.clone(portfolio)
        candidate_portfolio = Portfolio.clone(portfolio)

        for dt in pd.date_range(start_from, now, freq=('%dmin'%freq)):
            t = time.time()
            trades, next_portfolio = TradeDomain.get_trades(next_portfolio, simulated_pairs, dt, since)
            print('Trading %s: %f ms' % (Date.format(dt), (time.time()-t)*1000))
            trx_list += [trade.trx for trade in trades if trade.trx is not None]

        portfolio.set_total_in_exchange_currency(from_rates)
        candidate_portfolio.set_total_in_exchange_currency(now_rates)
        next_portfolio.set_total_in_exchange_currency(now_rates)
        return trx_list, portfolio, next_portfolio, candidate_portfolio

    @staticmethod
    def play_trading(portfolio, pairs=None, now=None, since=None):
        if now is None: now = Date.create()
        simulated_pairs = pairs if pairs is not None else IcoConnector.get_available_pairs()
        trades = []
        strategies = [
            [BasicDayTrading(), 1.0]
        ]

        for pair in simulated_pairs:
            data = IcoConnector.get_data(pair, since)
            stat_service = StatProviderService(pair, data, now)
            trade = BrokerService.calculate_pair_trade(strategies, stat_service)
            trades.append(trade)

        applied_trxs = []
        trades.sort()
        for trade in trades:
            if trade.trx is None: continue
            volume = BrokerService.calculate_trx_volume(trade.trx, portfolio)
            if volume == 0: continue
            trade.trx.set_volume_appending_fee(volume)
            try: portfolio = Portfolio.apply_trx(portfolio, trade.trx, now)
            except ValueError: break
            else: applied_trxs.append(trade.trx)

        return applied_trxs, portfolio


    # @staticmethod
    # def get_best_trxs(portfolio, pairs, now=None, since=None):
    #     pairTrades = TradeDomain.get_trades(pairs, now, since)
    #     return BrokerService._calculate_best_trxs(portfolio, pairTrades)
    #
    @staticmethod
    def get_rates(pairs=None, now=None, since=None):
        if now is None: now = Date.create()
        simulated_pairs = pairs if pairs is not None else IcoConnector.get_available_pairs()
        rates = {}
        for pair in simulated_pairs:
            data = IcoConnector.get_data(pair, since)
            stat_provider = StatProviderService(pair, data)
            rates[pair] = stat_provider.get_latest('close', now)
        return rates
