from collections import namedtuple

Ohlc = namedtuple('Basics', ['low', 'high', 'open', 'close'], verbose=True)
Tends = namedtuple('Tends', ['low_tend','high_tend'], verbose=True)
Levels = namedtuple('Levels', ['mrl','msl'], verbose=True)
Terms = namedtuple('Terms', ['short', 'medium', 'large'], verbose=True)
Heuristics = namedtuple('Heuristics', ['scales', 'weights', 'period'], verbose=True)
DailyStrategyMetrics =  namedtuple('DailyStategyMetrics', ['date', 'ohlc', 'weight'], verbose=True)