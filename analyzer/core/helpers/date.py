import os
from datetime import datetime, timedelta


class Date():
    default_format = '%Y-%m-%d %H:%M'
    api_format = '%Y%m%d%H%M'

    @staticmethod
    def create(str_date=None, format=default_format):
        """
        :param string date:
        :rtype: datetime
        """
        # In case CURRENT_TIME is defined on ENV variable, environment will run at that time in period
        if str_date is None: return datetime.now() if 'CURRENT_TIME' not in os.environ else Date.create(os.environ['CURRENT_TIME'])
        else: return datetime.strptime(str_date, format)

    @staticmethod
    def format(date, format=default_format):
        return date.strftime(format)

    @staticmethod
    def date_to_timestamp(date):
        """
        :param datetime date:
        :return int:
        """
        return int(date.timestamp())

    @staticmethod
    def timestamp_to_date(timestamp):
        return datetime.fromtimestamp(timestamp)

    @staticmethod
    def subtract(date, minutes=None, hours=None, days=None, months=None):
        if minutes is not None:
            date = date - timedelta(minutes=minutes)
        if hours is not None:
            date = date - timedelta(hours=hours)
        if days is not None:
            date = date - timedelta(days=days)
        if months is not None:
            date = date - timedelta(days=months*30)
        return date

    @staticmethod
    def add(date, minutes=None, hours=None, days=None, months=None):
        if minutes is not None:
            date = date + timedelta(minutes=minutes)
        if hours is not None:
            date = date + timedelta(hours=hours)
        if days is not None:
            date = date + timedelta(days=days)
        if months is not None:
            date = date + timedelta(days=months*30)
        return date