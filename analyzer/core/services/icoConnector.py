import pandas as pd
import numpy as np
import json
import urllib.request
import urllib.parse
import urllib.error

from django.conf import settings
from pip._vendor.requests.exceptions import HTTPError

from ..helpers.date import Date


class DataFaker():
    @staticmethod
    def calculate_low(value):
        return np.random.uniform(value - (np.random.random() * value), value)

    @staticmethod
    def calculate_high(value):
        return np.random.uniform(value, value + (np.random.random() * value))

    @staticmethod
    def calculate_open(low, high):
        return np.random.uniform(low, high)

    @staticmethod
    def calculate_bids_asks(nItem):
        return pd.Series(np.random.uniform(200.0, 290.0, nItem), index=list(range(nItem)), dtype='float32')

    @staticmethod
    def generate_history(pair, start, freq='D'):
        dates = pd.date_range(start, end=Date.create(), freq=freq)
        values = pd.DataFrame({
            'pair': pair,
            'close': None,
            'low': None,
            'high': None,
            'open': None,
            'aggregation': 1.0,
            'asks': None,
            'bids': None,
            'interval': 60,
            'time': [int(now.timestamp()) for now in dates],
        })

        values['close'] = np.random.uniform(200.0, 290.0, dates.size)
        values['low'] = [DataFaker.calculate_low(value) for value in values['close']]
        values['high'] = [DataFaker.calculate_high(value) for value in values['close']]
        values['open'] = [DataFaker.calculate_open(low, high) for low, high in zip(values['low'], values['high'])]
        values['asks'] = [DataFaker.calculate_bids_asks(10) for value in values['close']]
        values['bids'] = [DataFaker.calculate_bids_asks(10) for value in values['close']]
        return values.to_json(orient='records')

class DataApi():
    domain = settings.DATE_API_URL
    history_url = '/get-ohlc'
    pairs_url = '/get-pairs'
    get_history_data_cache = None
    get_available_pairs_cache = None

    response_name_mapping = {
        'close': 'close',
        'pair': 'pair',
        'high': 'high',
        'low': 'low',
        'open': 'open',
        'volume': 'volume',
        'time': 'time',
        'interval': 'interval',
        'bids': 'bids',
        'asks': 'asks'
    }

    @staticmethod
    def get_history_data(pair, since = None):
        if since is None: since = 'ALL'
        if DataApi.get_history_data_cache is None: DataApi.get_history_data_cache = {}
        if pair not in DataApi.get_history_data_cache: DataApi.get_history_data_cache[pair] = {}
        if since not in DataApi.get_history_data_cache[pair]: DataApi.get_history_data_cache[pair][since] = None
        if DataApi.get_history_data_cache[pair][since] is not None: return DataApi.get_history_data_cache[pair][since]

        params = {'pair': pair}
        if since != 'ALL': params['since'] = Date.format(since, Date.api_format)
        url = DataApi.domain + DataApi.history_url
        output = DataApi.call(url, params)
        for old, new in DataApi.response_name_mapping.items(): output = output.replace('"'+old+'"', '"'+new+'"')

        DataApi.get_history_data_cache[pair][since] = output
        return DataApi.get_history_data_cache[pair][since]

    @staticmethod
    def get_available_pairs():
        if DataApi.get_available_pairs_cache is not None: return DataApi.get_available_pairs_cache
        url = DataApi.domain + DataApi.pairs_url
        DataApi.get_available_pairs_cache = DataApi.call(url)
        return DataApi.get_available_pairs_cache

    @staticmethod
    def call(url, params = None, mode = 'GET'):
        if params is not None: params = urllib.parse.urlencode(params)
        try:
            if mode == 'GET':
                url_params = ('?'+params) if params is not None else ''
                print(url, url_params)
                req = urllib.request.urlopen(url + url_params)
            else: raise HTTPError('TODO')
        except urllib.error.HTTPError as err:
            raise Exception("Module DataApi: Error fetching data\n%s" % err.msg)

        output = req.read()
        encoding = req.info().get_content_charset('utf-8')
        return output.decode(encoding)


class IcoConnector():
    @staticmethod
    def get_fake_data(pair, start, freq='D'):
        json_data = DataFaker.generate_history(pair, start, freq)
        return json.loads(json_data)

    @staticmethod
    def get_data(pair, since = None):
        available_pairs = IcoConnector.get_available_pairs()
        if pair not in available_pairs: raise ValueError('Pair %s is not available' % pair)
        json_data = DataApi.get_history_data(pair, since)
        data = json.loads(json_data)
        return data

    @staticmethod
    def get_available_pairs():
        # return ['ETHEUR']
        json_data = DataApi.get_available_pairs()
        data = json.loads(json_data)
        return data


