import pandas as pd
from datetime import datetime
import math

from ..helpers.date import Date
from dateutil import rrule
import math
import numpy as np
from collections import OrderedDict

from ..helpers.tuples import Ohlc, Tends, Levels
from ..helpers.date import Date


class StatProviderService():
    pair = None
    data = None

    def __init__(self, pair, data, now=None):
        if len(data) == 0: raise ValueError('Cannot instantiate StatProviderService with empty data')
        self.pair = pair
        if now is not None:
            nowTs = Date.date_to_timestamp(now)
            data = list(filter(lambda x: x['time'] <= nowTs, data))

        str_dates = [ Date.timestamp_to_date(item['time']) for item in data]
        self.data = pd.DataFrame(data, index=str_dates)
        self.data['datetime'] = [Date.format(item) for item in str_dates]
        self.now = Date.timestamp_to_date(self.get_latest('time'))

    def get_data(self):
        return self.data

    def get_max(self, field, start=None, end=None):
        df = self.get_data()
        query = self._get_date_query(start, end)
        values = df.query(query) if query else df
        return values.max()[field]

    def get_min_where(self, field, query):
        df = self.get_data()
        values = df.query(query) if query else df
        return values.min()[field]

    def get_max_where(self, field, query):
        df = self.get_data()
        values = df.query(query) if query else df
        return values.max()[field]

    def get_min(self, field, start=None, end=None):
        df = self.get_data()
        query = self._get_date_query(start, end)
        values = df.query(query) if query else df
        return values.min()[field]

    def get_mean(self, field, start=None, end=None):
        df = self.get_data()
        query = self._get_date_query(start, end)
        values = df.query(query) if query else df
        return values.mean()[field]

    def get_latest(self, field, now = None):
        last_row = self._get_latest_metric(now)
        return last_row.mean()[field]

    def ohlc(self, interval):
        df = self.get_data()
        return df.resample('%dMin'%interval)\
            .agg({
                'time': 'min',
                'interval': 'max',
                'datetime': 'min',
                'open': 'first',
                'high': 'max',
                'low': 'min',
                'close': 'last',
                'volume': 'max',
                'pair': 'first'
            }).dropna(axis=0, how='any')

    def calculate_ohlc(self, date_from, date_to=None):
        """
        :param datetime date_to:
        :param int period:
        :rtype: ('low', 'high', 'open', 'close')
        """
        if date_to is None: date_to = self.now
        low = self.get_min('low', date_from, date_to)
        high = self.get_max('high', date_from, date_to)
        open = self.get_mean('open', date_from, date_to)
        close = self.get_mean('close', date_from, date_to)
        if low is None or math.isnan(low): raise ValueError('Period without data (%s, %s)' % (
            Date.format(date_from), Date.format(date_to)
        ))
        return Ohlc(low, high, open, close)

    def calculate_levels(self, from_date):
        itx = rrule.DAILY
        yesterday = Date.subtract(self.now, days=1)
        # daily_max, daily_min = list(), list()
        slot_size = self.get_latest('high', self.now) / 50
        price_min_slots = OrderedDict()
        price_max_slots = OrderedDict()
        for dt in rrule.rrule(itx, dtstart=from_date, until=yesterday):
            dt2 = Date.add(dt, hours=24)
            bottom_price, top_price = self.get_min('low', dt, dt2), self.get_max('high', dt, dt2)

            max_price_slot = math.trunc(top_price / slot_size) * slot_size
            min_price_slot = math.trunc(bottom_price / slot_size) * slot_size

            if max_price_slot not in price_max_slots: price_max_slots[max_price_slot] = 0
            if min_price_slot not in price_min_slots: price_min_slots[min_price_slot] = 0

            price_max_slots[max_price_slot] += 1
            price_min_slots[min_price_slot] += 1

        mrl = sorted([k for k, v in price_max_slots.items() if v > 1])
        msl = sorted([k for k, v in price_min_slots.items() if v > 1])
        return Levels(mrl, msl)

    def calculate_trends(self, from_date, tendency_n_days = 10):
        itx = rrule.DAILY
        yesterday = Date.subtract(self.now, days=1)
        price_mins, price_maxs = list(), list()
        for dt in rrule.rrule(itx, dtstart=from_date, until=yesterday):
            dt2 = Date.add(dt, hours=24)
            price_mins.append(self.get_min('low', dt, dt2))
            price_maxs.append(self.get_max('high', dt, dt2))

        line_min, line_max = [None, None], [None, None]
        min_angles, max_angles = [], []
        for idx, min, max in zip(range(0, len(price_mins)), price_mins, price_maxs):
            line_min[0], line_min[1] = line_min[1], [idx, min]
            line_max[0], line_max[1] = line_max[1], [idx, max]
            if line_min[0] is not None:
                min_angles.append(np.rad2deg(np.arctan2(line_min[1][1] - line_min[0][1], line_min[1][0] - line_min[0][0])))
            if line_max[0] is not None:
                max_angles.append(np.rad2deg(np.arctan2(line_max[1][1] - line_max[0][1], line_max[1][0] - line_max[0][0])))

        tendency_n_days = tendency_n_days if len(max_angles) > tendency_n_days else len(max_angles)
        max_tend = np.sum(max_angles[-tendency_n_days:]) / tendency_n_days / 90 # 90 Because is the max value of angels
        min_tend = np.sum(min_angles[-tendency_n_days:]) / tendency_n_days / 90
        return Tends(min_tend, max_tend)


    def _get_latest_metric(self, now = None):
        df = self.get_data()
        if now is None:
            last_ts = df.max()['time']
        else:
            ts = Date.date_to_timestamp(now)
            last_ts = df.query("time <= %s" % ts).max()['time']
        if math.isnan(last_ts):
            last_ts = df.max()['time']
            min_ts = df.min()['time']
            raise ValueError('There is not data for requested date %s. (Min,Max)(%s, %s)' % (\
                Date.format(now), Date.format(Date.timestamp_to_date(min_ts)), Date.format(Date.timestamp_to_date(last_ts))\
            ))
        query = "time == %s" % (last_ts)
        return df.query(query)

    def _get_date_query(self, start = None, end = None):
        """
        :type start: datetime
        :type end: datetime
        :return str:
        """
        query_param = []
        if start is not None:
            query_param.append('time >= ' + str(Date.date_to_timestamp(start)))
        if end is not None:
            query_param.append('time <= ' + str(Date.date_to_timestamp(end)))
        return ' & '.join(query_param)
