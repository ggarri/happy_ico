from ..services.statProviderService import StatProviderService

from ..strategy.strategyAbstract import StrategyAbstract

from ..models.portfolio import Portfolio
from ..models.portfolio import Trx
from ..models.trade import Trade

from ..helpers.date import Date


class BrokerService:
    trade_margin_threshold = 0.025
    min_volume_per = 0.01 # 1% of the price
    # price_trx_min_volume = 0.10 # Percentage of Currency to trade
    trading_volume_per = 1

    @staticmethod
    def calculate_pair_trade(strategies, stat_service):
        """
        :param StatProviderService stat_service:
        :rtype: Trade
        """
        trade = Trade(stat_service.pair)
        for strategyItem in strategies:
            strategy_variables = BrokerService._get_strategy_variables(stat_service, strategyItem[0], strategyItem[1])
            trade.append_strategy_variables(strategyItem[0].__class__.__name__, strategy_variables)

        trx = BrokerService._get_trade_trx(trade, stat_service)
        trade.set_trx(trx)
        return trade

    @staticmethod
    def calculate_trx_volume(trx, portfolio):
        """
        :param Trx trx:
        :param Portfolio portfolio:
        :rtype float:
        """
        op_currency = Portfolio.extract_op_currency(trx.pair)
        exchange_currency = portfolio.get_exchange_currency()

        if trx.action == Trx.ACTION_SELL:  # It is selling all, but in need @TODO improve
            volume = portfolio.get_wallet_currency(op_currency) * BrokerService.trading_volume_per
        elif trx.action == Trx.ACTION_BUY:
            exchange_amount = portfolio.get_wallet_currency(exchange_currency) * BrokerService.trading_volume_per
            action_fee = exchange_amount * trx.PER_FEE
            volume = (exchange_amount - action_fee) / trx.rate
        else:
            raise ValueError('Trx action is not set')

        # min_trading_volume = trx.rate * BrokerService.min_volume_per
        return 0 if volume < 0.01 else volume

    @staticmethod
    def _get_strategy_variables(stat_service, strategyClass, strategyWeight):
        """
        :param StatProviderService stat_service:
        :param StrategyAbstract strategyClass:
        :param float strategyWeight: [0,1]
        :rtype StrategyOutcome:
        """
        strategy_variables = strategyClass.calculate_variables(stat_service=stat_service)
        strategy_variables.set_weight(strategyWeight)
        return strategy_variables

    @staticmethod
    def _get_trade_trx(trade, stat_service):
        """
        :param Trade trade:
        :param StatProviderService stat_service:
        :rtype: Trx
        """

        action = trade.get_trx_action()
        trade_margin = trade.get_trade_margin(action)
        if trade_margin < BrokerService.trade_margin_threshold: return None

        trx = Trx.create_template(pair=trade.pair,
                                  rate=stat_service.get_latest('close'),
                                  action=action,
                                  timestamp=Date.date_to_timestamp(stat_service.now))

        return trx

    # @staticmethod
    # def _acceptance_trx(trx, min_volume_per):
    #     """
    #     :param Trx trx:
    #     :return:
    #     """
    #     volume_price = trx.volume * trx.rate
    #     min_volume = trx.rate * min_volume_per
    #     if volume_price < min_volume: return False
    #     return True
