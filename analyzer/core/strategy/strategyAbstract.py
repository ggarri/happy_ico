from ..models.strategyOutcome import StrategyOutcome
from ..services.statProviderService import StatProviderService

class StrategyAbstract():
    acceptance_trade_threshold = 0.15 # 1.5%

    @staticmethod
    def calculate_variables(stat_service):
        """
        :param StatProviderService stat_service:
        :rtype: StrategyOutcome
        """
        raise BaseException('Missing implementation')

    def _normilize_scale(self, scales, scale_v):
        if scale_v <= 0: return 0
        if scale_v > scales: return scales
        return scale_v
