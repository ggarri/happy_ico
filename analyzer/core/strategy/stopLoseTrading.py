from .strategyAbstract import StrategyAbstract
from ..models.strategyOutcome import StrategyOutcome

class StopLostTrading(StrategyAbstract):
    heuristics = {
        'loses_in_short': 0.6,
    }

    @staticmethod
    def calculate_variables(pair, stat_service):
        """
        :parent
        """
        self = StopLostTrading()
        return self._get_pair_trade(pair, stat_service)


    def _get_pair_trade(self, pair, pair_metrics):
        """
        parent:
        """

        profit_per = self._calculate_profit(self.heuristics, pair_metrics)
        lost_per = self._calculate_lost(self.heuristics, pair_metrics)
        up_odds, down_odds = self._calculate_odds(self.heuristics, pair_metrics)

        return StrategyOutcome(
            pair=pair,
            profit_per=profit_per,
            lost_per=lost_per,
            up_odds=up_odds,
            down_odds=down_odds
        )

    # @TODO Use MSL (Resistence Level)
    def _calculate_profit(self, heuristics, pair_metrics):
        """
        :param PairHistoricalMetrics pair_metrics:
        :rtype float
        """
        return 0.1

    # @TODO Use MRL (Support Level)
    def _calculate_lost(self, heuristics, pair_metrics):
        """
        :rtype float
        """
        return 1.0

    # @TODO Use pair tendency and triangle type to define risk and odds
    def _calculate_odds(self, heuristics, pair_metrics):
        """
        :param PairHistoricalMetrics pair_metrics:
        :rtype float
        """
        max_lost_in_short, max_lost_in_medium, max_lost_in_large, min_lost_in_short, min_lost_in_medium, min_lost_in_large\
            = StopLostTrading.get_class_metrics(pair_metrics)

        up_odds, down_odds = 1, 1

        return up_odds, down_odds

    @staticmethod
    def get_class_metrics(pair_metrics):
        """
        :param PairHistoricalMetrics pair_metrics:
        """
        max_lost_in_short = pair_metrics.max_instant_medium_term_close - pair_metrics.max_instant_short_term_close / pair_metrics.max_instant_medium_term_close
        max_lost_in_medium = pair_metrics.max_instant_large_term_close - pair_metrics.max_instant_short_term_close / pair_metrics.max_instant_large_term_close
        max_lost_in_large = pair_metrics.max_short_term_close - pair_metrics.max_instant_short_term_close / pair_metrics.max_short_term_close

        min_lost_in_short = pair_metrics.min_instant_medium_term_close - pair_metrics.min_instant_short_term_close / pair_metrics.min_instant_medium_term_close
        min_lost_in_medium = pair_metrics.min_instant_large_term_close - pair_metrics.min_instant_short_term_close / pair_metrics.min_instant_large_term_close
        min_lost_in_large = pair_metrics.min_short_term_close - pair_metrics.min_instant_short_term_close / pair_metrics.min_short_term_close
        return max_lost_in_short, max_lost_in_medium, max_lost_in_large, min_lost_in_short, min_lost_in_medium, min_lost_in_large



