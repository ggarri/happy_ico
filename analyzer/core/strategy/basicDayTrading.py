import math

from .strategyAbstract import StrategyAbstract

from ..services.statProviderService import StatProviderService
from ..models.strategyOutcome import StrategyOutcome

from ..helpers.tuples import Heuristics, DailyStrategyMetrics, Ohlc
from ..helpers.date import Date


def weigh_day_metric(metric):
    """
    :param Ohlc metric:
    :rtype float:
    """
    return (metric.high + metric.low + metric.open + metric.close)/4

class BasicDayTrading(StrategyAbstract):
    heuristics = Heuristics(
        [0.9, 0.8, 0.7, 0.3, 0.2, 0.1], # Scales
        [0.5, 0.3, 0.2], # Weights
        [{'hours': 2}, {'hours': 12}, {'days': 1}], # Period
    )

    @staticmethod
    def calculate_variables(stat_service, now = None):
        """
        :param str pair:
        :param StatProviderService stat_service:
        :return:
        """
        if now is None: now = stat_service.now

        self = BasicDayTrading()
        current_metrics = stat_service.calculate_ohlc(now, now)
        past_metrics = []
        for period, weight in zip(self.heuristics.period, self.heuristics.weights):
            date_from = Date.subtract(now, **period)
            past_metrics.append(
                DailyStrategyMetrics(date_from, stat_service.calculate_ohlc(date_from, now), weight)
            )

        return self._get_pair_trade(stat_service.pair, current_metrics, past_metrics)

    def _get_pair_trade(self, pair, cur_metric, past_metrics):
        """
        :param str pair:
        :param Ohlc cur_metric:
        :param list(DailyStrategyMetrics) past_metrics:
        :return:
        """
        profit_per = self._calculate_profit(cur_metric, past_metrics)
        lost_per = self._calculate_lost(cur_metric, past_metrics)
        up_odds, down_odds,  = self._calculate_odds(cur_metric, past_metrics)

        return StrategyOutcome(
            pair=pair,
            profit_per=profit_per,
            lost_per=lost_per,
            up_odds=up_odds,
            down_odds=down_odds
        )

    def _calculate_profit(self, cur_metric, past_metrics):
        """
        :param Ohlc cur_metric:
        :param list(DailyStrategyMetrics) past_metrics:
        :rtype float
        """
        estimation = 0.0
        for past_metric in past_metrics:
            profit_odds = (past_metric.ohlc.high - cur_metric.close) / cur_metric.close
            estimation += profit_odds * past_metric.weight
        return estimation

    def _calculate_lost(self, cur_metric, past_metrics):
        """
        :param Ohlc cur_metric:
        :param list(DailyStrategyMetrics) past_metrics:
        :rtype float
        """
        estimation = 0.0
        for past_metric in past_metrics:
            lost_odds = (cur_metric.close - past_metric.ohlc.low) / cur_metric.close
            estimation += lost_odds * past_metric.weight
        return estimation

    def _calculate_odds(self, cur_metric, past_metrics):
        """
        :param Ohlc cur_metric:
        :param list(DailyStrategyMetrics) past_metrics:
        :rtype float
        """

        scales = len(self.heuristics.scales)-1

        def get_metric_slot(past_metric, latest_metric):
            """
            :param Ohlc past_metric:
            :param Ohlc latest_metric:
            :rtype: float
            """
            high = max(past_metric.high, latest_metric.high)
            low = min(past_metric.low, latest_metric.low)
            percentile = (high - latest_metric.close) / (high - low)
            return self._normilize_scale(scales, math.floor(percentile / (1.0 / scales)))

        up_odds, down_odds = 0.0, 0.0
        for past_metric in past_metrics:
            metric_slot = get_metric_slot(past_metric.ohlc, cur_metric)
            up_odds += (self.heuristics.scales[scales - metric_slot] * past_metric.weight)
            down_odds += (self.heuristics.scales[metric_slot] * past_metric.weight)

        return up_odds, down_odds



