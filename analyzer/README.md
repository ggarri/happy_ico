## Set environment

### OS Dependencies
Install next OS dependencies for your OS

- python3.4
- python-setuptools
- python-pip
- virtualenv

### Step 1
Install virtualenvwrapper
```
$ sudo pip install virtualenvwrapper
```

### Step 2
Configure our first virtualenv
```
$ export WORKON_HOME=$HOME/.virtualenvs
$ source /usr/local/bin/virtualenvwrapper.sh
$ mkdir -p $WORKON_HOME
```

To make ENV permanent add first two commands within `$HOME/.bashrc`

### Step 3
Create local virtual enviroment with python 3.4
```
mkdir $WORKON_HOME/{virtual_env_name}
$ mkvirtualenv -p /usr/bin/python3.4 --no-site-packages --distribute $WORKON_HOME/{virtual_env_name}
```

Once you excecuted above command you are gonna be inside the virtual dev env. In case you want to load it again, run next command:
```
workon {virtual_env_name}
```

### Step 4 
Install project requirements
```
pip install -r ./requirements.txt --upgrade
```

### To include new packages (OPTIONAL)
To install a new package to dependencies:
```
pip install {poackage_name}
pip freeze > ./requirements.txt
```

### Using iPython on virtualenv version
To get ipython running using virtualenv python version, you would need to create an alias
```
alias ipy="python -c 'import IPython; IPython.terminal.ipapp.launch_new_instance()'"
```
So now if you run ipy you would get local iPython shell running on local env python version

## Run DEV server
```
> workon {env_name}
> python manage.py runserver
```